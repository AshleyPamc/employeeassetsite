import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../Service/app.service';
import { Assets } from '../../Models/assets';
import { AssetDetails } from '../../Models/asset-details';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AssetDetailFilter } from '../../Filters/AssetDetailFilter';
import { AssetRegIdFilter } from '../../Filters/AssetRegIdFilter';
import { AssetDescriptionFilter } from '../../Filters/AssetDescriptionFilter';
import { AssetCellFilter } from '../../Filters/AssetCellFilter';
import { AssetFriendlyFilter } from '../../Filters/AssetFriendlyFilter';

@Component({
  selector: 'app-assets',
  templateUrl: './assets.component.html',
  styleUrls: ['./assets.component.css']
})
export class AssetsComponent implements OnInit {

  private _assetDet: boolean = false;
  private _assets: boolean = false;
  private _editAsset: boolean = false;
  private _editAssetDet: boolean = false;
  private _newAsset: boolean = false;
  private _newAssetDet: boolean = false;
  private _selectedAsset: Assets = new Assets();
  private _selectedAssetDet: AssetDetails = new AssetDetails();
  private assetDetailFilter = new AssetDetailFilter();
  private assetRegIdFilter = new AssetRegIdFilter();
  private assetDescriptionFilter = new AssetDescriptionFilter();
  private assetCellFilter = new AssetCellFilter();
  private assetFriendlyFilter = new AssetFriendlyFilter()

  @Input() assetForm: FormGroup;
  @Input() assetDetForm: FormGroup;


  constructor(public _appService: AppService, private fb: FormBuilder, private fba: FormBuilder) { }

  ngOnInit() {

    this.assetForm = this.fb.group({
      'regid': [],
      'type': [],
      'cell': [],
      'frName':[]
    })

    this.assetDetForm = this.fba.group({
      'desc': [],
    })
  }

  public get selectedAsset(): Assets {
    return this._selectedAsset;
  }
  public set selectedAsset(value: Assets) {
    this._selectedAsset = value;
  }
  public get selectedAssetDet(): AssetDetails {
    return this._selectedAssetDet;
  }
  public set selectedAssetDet(value: AssetDetails) {
    this._selectedAssetDet = value;
  }
  public get assetDet(): boolean {
    return this._assetDet;
  }
  public set assetDet(value: boolean) {
    this._assetDet = value;
  }
  public get editAsset(): boolean {
    return this._editAsset;
  }
  public set editAsset(value: boolean) {
    this._editAsset = value;
  }
  public get editAssetDet(): boolean {
    return this._editAssetDet;
  }
  public set editAssetDet(value: boolean) {
    this._editAssetDet = value;
  }
  public get newAsset(): boolean {
    return this._newAsset;
  }
  public set newAsset(value: boolean) {
    this._newAsset = value;
  }
  public get newAssetDet(): boolean {
    return this._newAssetDet;
  }
  public set newAssetDet(value: boolean) {
    this._newAssetDet = value;
  }
  public get assets(): boolean {
    return this._assets;
  }
  public set assets(value: boolean) {
    this._assets = value;
  }

  ShowAssetDetail() {
    let c = document.getElementById("asset");
    let b = document.getElementById("detail");


    c.style.backgroundColor = "#FAFAFA";
    b.style.backgroundColor = "#C1CDD4";


    this._assets = false;
    this._assetDet = true;

    this._appService.TransactionService.GetAssetsDetails();
  }
  ShowAssets() {
    let c = document.getElementById("asset");
    let b = document.getElementById("detail");


    b.style.backgroundColor = "#FAFAFA";
    c.style.backgroundColor = "#C1CDD4";


    this._assetDet = false;
    this._assets = true;

    this._appService.TransactionService.GetAssets();
  }

  DeleteAsset(data: Assets) {
    let c = <HTMLInputElement>document.getElementById("aDelete");

    data.deleted = c.checked;
    data.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    this._appService.TransactionService.DeleteAssets(data);
  }

  EditAsset(data: Assets) {
    this._appService.TransactionService.GetAssetsDetails();
    this.editAsset = true;
    this.assetForm.reset();
    this.selectedAsset = new Assets();
    this.selectedAsset = data;
    this.assetForm.controls['regid'].setValue(data.regId);
    this.assetForm.controls['type'].setValue(data.type);
    this.assetForm.controls['cell'].setValue(data.cell);
    this.assetForm.controls['frName'].setValue(data.frName);
  }

  UpdateAsset() {
    
    let d: Assets = new Assets();
    d.cell = this.assetForm.get('cell').value;
    d.cell = this.assetForm.get('cell').value;
    if (d.cell == null || d.cell == undefined || d.cell == '') {
      d.cell = " ";
    }
    d.type = this.assetForm.get('type').value;
    d.regId = this.assetForm.get('regid').value;
    d.frName = this.assetForm.get('frName').value;
    d.id = this.selectedAsset.id;
    d.createBy = this._appService.LoginService.CurrentLoggedInUser.username;
    d.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    this.assetForm.reset();

    this._appService.TransactionService.UpdateAssets(d);

    this.editAsset = false;

  }

  NewAsset() {
    
    let d: Assets = new Assets();
    d.cell = this.assetForm.get('cell').value;
    if (d.cell == null || d.cell == undefined || d.cell == '') {
      d.cell = " ";
    }
    d.type = this.assetForm.get('type').value;
    d.regId = this.assetForm.get('regid').value;
    d.frName = this.assetForm.get('frName').value;
    d.createBy = this._appService.LoginService.CurrentLoggedInUser.username;
    d.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    this.assetForm.reset();

    this._appService.TransactionService.NewAssets(d);
    this.newAsset = false;
  }

  OpenNewAsset() {
    this._appService.TransactionService.GetAssetsDetails();
    this.newAsset = true;
    this.assetForm.reset();
  }

  DeleteAssetDetail(data: AssetDetails) {
    let c = <HTMLInputElement>document.getElementById("adDelete");

    data.deleted = c.checked;
    data.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    this._appService.TransactionService.DeleteAssetsDetails(data);
  }

  EditAssetDetail(data: AssetDetails) {
    this.editAssetDet = true;
    this.assetForm.reset();
    this.selectedAsset = new Assets();
    this.selectedAssetDet = data;
    this.assetDetForm.controls['desc'].setValue(data.desc);

  }

  UpdateAssetDetail() {
    
    let d: AssetDetails = new AssetDetails();
    d.desc = this.assetDetForm.get('desc').value;
    d.type = this.selectedAssetDet.type;
    d.createBy = this._appService.LoginService.CurrentLoggedInUser.username;
    d.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    this.assetDetForm.reset();

    this._appService.TransactionService.UpdateAssetsDetails(d);

    this.editAssetDet = false;
  }

  NewAssetDetail() {
    
    let d: AssetDetails = new AssetDetails();
    d.desc = this.assetDetForm.get('desc').value;
    d.createBy = this._appService.LoginService.CurrentLoggedInUser.username;
    d.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    this.assetDetForm.reset();

    this._appService.TransactionService.NewAssetsDetails(d);
    this.newAssetDet = false;
  }

  OpenNewAssetDetail() {
    this.newAssetDet = true;
    this.assetDetForm.reset();
  }

  ExportAssetList() {
    this._appService.TransactionService.ExportAssetList();
  }

}
