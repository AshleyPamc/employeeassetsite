import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../Service/app.service';
import { AssetAssign } from '../../Models/asset-assign';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Employees } from '../../Models/employees';
import { Assets } from '../../Models/assets';
import { EmployeeNameFilter } from '../../Filters/EmployeeNameFilter';
import { AssetDescriptionFilter } from '../../Filters/AssetDescriptionFilter';
import { AssetRegIdFilter } from '../../Filters/AssetRegIdFilter';
import { AssetAssignNameFilter } from '../../Filters/AssetAssignNameFilter';
import { AssetAssignTypeFilter } from '../../Filters/AssetAssignTypeFilter';
import { AssetAssignFrNameFilter } from '../../Filters/AssetAssignFrNameFilter';
import { AssignFromToDateFilter } from '../../Filters/AssignFromToDateFilter';
import { AssetFriendlyFilter } from '../../Filters/AssetFriendlyFilter';
import { AssetCellFilter } from '../../Filters/AssetCellFilter';
import { EmployeeSurnameFilter } from '../../Filters/EmployeeSurnameFilter';

@Component({
  selector: 'app-assign-assets',
  templateUrl: './assign-assets.component.html',
  styleUrls: ['./assign-assets.component.css']
})
export class AssignAssetsComponent implements OnInit {

  private _assignErr: boolean = false;
  private _assetAssign: boolean = false;
  private _unassign: boolean = false;
  private _selectedAssetAssign: AssetAssign = new AssetAssign();
  private _openEdit: boolean = false;
  private _openNew: boolean = false;
  private _selectedEmp: Employees = new Employees();
  private _selectedAsset: Assets = new Assets();
  public busy: boolean = false;
  private _openDates: boolean = false;
  private employeeNameFilter = new AssetAssignNameFilter();
  private empNameFilter = new EmployeeNameFilter();
  private empSurFilter = new EmployeeSurnameFilter();
  private assetDescriptionFilter = new AssetAssignTypeFilter();
  private assetRegIdFilter = new AssetRegIdFilter();
  private assetFriendlyFilter = new AssetAssignFrNameFilter();
  private assignDateFilter = new AssignFromToDateFilter();
  private assetRegId = new AssetRegIdFilter();
  private assetFrName = new AssetFriendlyFilter();
  private assetName = new AssetDescriptionFilter();
  private assetCell = new AssetCellFilter();





  @Input() assignForm: FormGroup;


  constructor(public _appService: AppService, private fb: FormBuilder) { }

  ngOnInit() {

    this.assignForm = this.fb.group({
      'employee': [],
      'asset': [],
      'from': [],
      'to': []
      
    })
  }

  public get assignErr(): boolean {
    return this._assignErr;
  }
  public set assignErr(value: boolean) {
    this._assignErr = value;
  }
  public get openDates(): boolean {
    return this._openDates;
  }
  public set openDates(value: boolean) {
    this._openDates = value;
  }
  public get selectedAsset(): Assets {
    return this._selectedAsset;
  }
  public set selectedAsset(value: Assets) {
    this._selectedAsset = value;
  }
  public get selectedEmp(): Employees {
    return this._selectedEmp;
  }
  public set selectedEmp(value: Employees) {
    this._selectedEmp = value;
  }
  public get openNew(): boolean {
    return this._openNew;
  }
  public set openNew(value: boolean) {
    this._openNew = value;
  }
  public get openEdit(): boolean {
    return this._openEdit;
  }
  public set openEdit(value: boolean) {
    this._openEdit = value;
  }
  public get unassign(): boolean {
    return this._unassign;
  }
  public set unassign(value: boolean) {
    this._unassign = value;
  }
  public get assetAssign(): boolean {
    return this._assetAssign;
  }
  public set assetAssign(value: boolean) {
    this._assetAssign = value;
  }
  public get selectedAssetAssign(): AssetAssign {
    return this._selectedAssetAssign;
  }
  public set selectedAssetAssign(value: AssetAssign) {
    this._selectedAssetAssign = value;
  }

  ShowAssetAssign() {
    let c = document.getElementById("asset");
    let b = document.getElementById("unassign");


    b.style.backgroundColor = "#FAFAFA";
    c.style.backgroundColor = "#C1CDD4";


    this._unassign = false;
    this._assetAssign = true;

    this._appService.TransactionService.GetEmployees();
    this._appService.TransactionService.GetUnAssignedEmployess();
    this._appService.TransactionService.GetUnAssignedAssets();
    this._appService.TransactionService.GetAssets();
    this._appService.TransactionService.GetAssetsAssign();
    this.selectedAsset = new Assets();
    this.selectedEmp = new Employees();

    this.busy = true;
    setTimeout((a) => {
      this._appService.TransactionService.listAssets.forEach((el) => {
        let found = false;
        this._appService.TransactionService.unassignedAssets.forEach((e) => {
          if (e.id == el.id) {
            found = true;
          }
        });

        if (!found) {
          el.assigned = true;
        }
      })
      this.busy = false;
    }, 1000);
    this.busy = true;
    setTimeout((a) => {
      this._appService.TransactionService.employeeList.forEach((el) => {
        let found = false;
        this._appService.TransactionService.unassignedEmp.forEach((e) => {
          if (e.id == el.id) {
            found = true;
          }
        });

        if (!found) {
          el.assigned = true;
        }
      })
      this.busy = false;
    }, 1000);

  }
  ShowUnassign() {
    let c = document.getElementById("asset");
    let b = document.getElementById("unassign");


    c.style.backgroundColor = "#FAFAFA";
    b.style.backgroundColor = "#C1CDD4";


    this._unassign = true;
    this._assetAssign = false;

    this._appService.TransactionService.GetUnAssignedAssets();
   
  }

  EditAssetAssign(data: AssetAssign) {

    this._selectedAssetAssign = new AssetAssign();
    this._selectedAssetAssign = data;
    this._openEdit = true;

    this.assignForm.reset();

    this.assignForm.controls['from'].setValue(data.fromDate.substr(0, 10));
    this.assignForm.controls['from'].disable();
    this.assignForm.controls['to'].setValue(data.toDate.substr(0, 10));

  }
  
  DeleteAssetAssign(data: AssetAssign) {

    this._appService.TransactionService.DeleteAssetsAssign(data);
  }

  UpdateAssetAssign() {
    let d: AssetAssign = new AssetAssign();
    d.fromDate = this.assignForm.get('from').value;
    d.toDate = this.assignForm.get('to').value;
    d.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    d.createBy = this._appService.LoginService.CurrentLoggedInUser.username;
    d.id = this._selectedAssetAssign.id;

    this._openEdit = false;
    this.assignForm.reset();
    this._appService.TransactionService.UpdateAssetsAssign(d);


  }

  OpenNew() {
    this._openNew = true;
    this._appService.TransactionService.GetEmployees();
    this._appService.TransactionService.GetUnAssignedEmployess();
    this._appService.TransactionService.GetUnAssignedAssets();
    this._appService.TransactionService.GetAssets();
    this._appService.TransactionService.GetAssetsAssign();
    this.selectedAsset = new Assets();
    this.selectedEmp = new Employees();

    this.busy = true;
    setTimeout((a) => {
      this._appService.TransactionService.listAssets.forEach((el) => {
        let found = false;
        this._appService.TransactionService.unassignedAssets.forEach((e) => {
          if (e.id == el.id) {
            found = true;
          }
        });

        if (!found) {
          el.assigned = true;
        }
      })
      this.busy = false;
    }, 1000);
    this.busy = true;
    setTimeout((a) => {
      this._appService.TransactionService.employeeList.forEach((el) => {
        let found = false;
        this._appService.TransactionService.unassignedEmp.forEach((e) => {
          if (e.id == el.id) {
            found = true;
          }
        });

        if (!found) {
          el.assigned = true;
        }
      })
      this.busy = false;
    }, 1000);
   // this.assignForm.reset();
  }

  AddNewDates() {
    let d: AssetAssign = new AssetAssign();
    d.empId = this.selectedEmp.id;
    d.assetId = this.selectedAsset.id;
    d.fromDate = this.assignForm.get('from').value;
    d.toDate = this.assignForm.get('to').value;
    if (d.toDate == null || d.toDate == undefined || d.toDate == '') {
      d.toDate = "";
    }
    d.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    d.createBy = this._appService.LoginService.CurrentLoggedInUser.username;


    this._openNew = false;
    this._openDates = false;
    this.assignForm.reset();
    this._appService.TransactionService.NewAssetsAssign(d);
  }

  SelectEmployee(data: Employees,e) {
    let c = e.target.checked;
    if (c) {
      this.selectedEmp = new Employees();
      this.selectedEmp = data;
      this._appService.TransactionService.employeeList.forEach((el) => {
        if (el.id == this.selectedEmp.id) {
          el.selected = true;
          if (el.assigned) {
            this.assignErr = true;
          }
        } else {
          el.selected = false;
        }
      });
    } else {
      this.selectedEmp = new Employees();
      this._appService.TransactionService.employeeList.forEach((el) => {

        el.selected = false;

      });
    }
  }

  SelectAsset(data: Assets, e) {
    let c = e.target.checked;
    if (c) {
      this.selectedAsset = new Assets();
      this.selectedAsset = data;
      this._appService.TransactionService.listAssets.forEach((el) => {
        if (el.id == this.selectedAsset.id) {
          el.selected = true;
          if (el.assigned) {
            this.assignErr = true;
          }
        } else {
          el.selected = false;
        }
      });
    } else {
      this.selectedAsset = new Assets();
      this._appService.TransactionService.listAssets.forEach((el) => {

        el.selected = false;

      });
    }
  }

}
