import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../Service/app.service';
import { DashFrNameFilter } from '../../Filters/DashFrNameFilter';
import { DashAssetFilter } from '../../Filters/DashAssetFilter';
import { DashNameFilter } from '../../Filters/DashNameFilter';
import { DashDateFilter } from '../../Filters/DashDateFilter';
import { DashDepFilter } from '../../Filters/DashDepFilter';
import { DashWorkGroupFilter } from '../../Filters/DashWorkGroupFilter';
import { DashRegIdFilter } from '../../Filters/DashRegIdFilter';
import { ExcelListModel } from '../../Models/excel-list-model';
import { AssetAssign } from '../../Models/asset-assign';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  private dashNameFilter = new DashNameFilter();
  private dashFrNameFilter = new DashFrNameFilter();
  private dashAssetFilter = new DashAssetFilter();
  private dashDateFilter = new DashDateFilter();
  private dashDepFilter = new DashDepFilter();
  private dashWorkGroupFilter = new DashWorkGroupFilter();
  private dashRegIdFilter = new DashRegIdFilter();
  private _openHist: boolean = false;
  private _lapCount: number = 0;
  private _wifiCount: number = 0;
  private _uncapCount: number = 0;
  public openEdit: boolean = false;
  public selectedAssign: AssetAssign = new AssetAssign();

  constructor(public _appService: AppService, private fb: FormBuilder) { }

  public get openHist(): boolean {
    return this._openHist;
  }
  public set openHist(value: boolean) {
    this._openHist = value;
  }

  @Input() assignForm: FormGroup;



  ngOnInit() {

    this.assignForm = this.fb.group({
      'employee': [],
      'asset': [],
      'from': [],
      'to': []

    })
  }
  SortLaptopField() {
    if (this._lapCount < 1) {
      this._appService.TransactionService.dashList = this._appService.TransactionService.dashList.sort((a, b) => a.laptop > b.laptop ? -1 : 1);
      this._lapCount++;
    } else {
      this._appService.TransactionService.dashList = this._appService.TransactionService.dashList.sort((a, b) => b.laptop > a.laptop ? -1 : 1);
      this._lapCount = 0
    }
  }
  SortWifiField() {
    if (this._wifiCount < 1) {
      this._appService.TransactionService.dashList = this._appService.TransactionService.dashList.sort((a, b) => a.wifi > b.wifi ? -1 : 1);
      this._wifiCount++;
    } else {
      this._appService.TransactionService.dashList = this._appService.TransactionService.dashList.sort((a, b) => b.wifi > a.wifi ? -1 : 1);
      this._wifiCount = 0
    }
  }
  SortUncapField() {
    if (this._uncapCount < 1) {
      this._appService.TransactionService.dashList = this._appService.TransactionService.dashList.sort((a, b) => a.uncaped > b.uncaped ? -1 : 1);
      this._uncapCount++;
    } else {
      this._appService.TransactionService.dashList = this._appService.TransactionService.dashList.sort((a, b) => b.uncaped > a.uncaped ? -1 : 1);
      this._uncapCount = 0
    }
  }

  GetAssetHistory(data: ExcelListModel) {
    this._appService.TransactionService.GetAssetsAssignHist(data);
    this._openHist = true;
  }

  OpenEdit(data: AssetAssign) {
    this.selectedAssign = new AssetAssign();
    this.selectedAssign = data;

    this.assignForm.controls['from'].setValue(data.fromDate.substr(0, 10));
    this.assignForm.controls['from'].disable();
    this.assignForm.controls['to'].setValue(data.toDate.substr(0, 10));
    this.openEdit = true;
    
  }

  UpdateAssetAssign() {
    let d: AssetAssign = new AssetAssign();
    d.fromDate = this.assignForm.get('from').value;
    d.toDate = this.assignForm.get('to').value;
    d.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    d.createBy = this._appService.LoginService.CurrentLoggedInUser.username;
    d.id = this.selectedAssign.id;

    this.openEdit = false;
    this.assignForm.reset();
    this._appService.TransactionService.UpdateAssetsAssign(d);


  }
}
