import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../Service/app.service';
import { Departments } from '../../Models/departments';
import { Groups } from '../../Models/groups';
import { FormGroup, FormBuilder } from '@angular/forms';
import { Employees } from '../../Models/employees';
import { EmployeeNameFilter } from '../../Filters/EmployeeNameFilter';
import { EmployeeDepFilter } from '../../Filters/EmployeeDepFilter';
import { EmployeeSurnameFilter } from '../../Filters/EmployeeSurnameFilter';
import { EmployeeWorkGroupFilter } from '../../Filters/EmployeeWorkGroupFilter';
import { DepartmentFilter } from '../../Filters/DepartmentFilter';
import { WorkGroupFilter } from '../../Filters/WorkGroupFilter';


@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  private _lapCount: number = 0;
  private _wifiCount: number = 0;
  private _uncapCount: number = 0;
  private employeeNameFilter = new EmployeeNameFilter();
  private employeeDepFilter = new EmployeeDepFilter();
  private employeeSurnameFilter = new EmployeeSurnameFilter();
  private employeeWorkGroupFilter = new EmployeeWorkGroupFilter();
  private customStringFilter = new DepartmentFilter();
  private customStringFilterG = new WorkGroupFilter();
  private _employees: boolean = false;
  private _workGroup: boolean = false;
  private _department: boolean = false;
  private _selectedDep: Departments = new Departments();
  private _selectedGroup: Groups = new Groups();
  private _editDep: boolean = false;
  private _editGroup: boolean = false;
  private _editEmp: boolean = false;
  private _selectedEmp: Employees = new Employees();
  private _newEmp: boolean = false;
  private _newDep: boolean = false;
  private _newGroup: boolean = false;


  @Input() depForm: FormGroup;
  @Input() groupForm: FormGroup;
  @Input() empForm: FormGroup;

  constructor(public _appService: AppService, private fb: FormBuilder, private fbg: FormBuilder, private fbe: FormBuilder) {
    this._appService.TransactionService.GetGroup();
  }

  ngOnInit() {
    this.depForm = this.fb.group({
      'description': [],
    });
    this.groupForm = this.fbg.group({
      'description': [],
    });
    this.empForm = this.fbe.group({
      'name': [],
      'surname': [],
      'cell': [],
      'email': [],
      'dep': [],
      'grp': [],
      'laptop': [],
      'wifi': [],
      'uncaped': []
    })
  }


  public get newGroup(): boolean {
    return this._newGroup;
  }
  public set newGroup(value: boolean) {
    this._newGroup = value;
  }
  public get newDep(): boolean {
    return this._newDep;
  }
  public set newDep(value: boolean) {
    this._newDep = value;
  }
  public get newEmp(): boolean {
    return this._newEmp;
  }
  public set newEmp(value: boolean) {
    this._newEmp = value;
  }
  public get editEmp(): boolean {
    return this._editEmp;
  }
  public set editEmp(value: boolean) {
    this._editEmp = value;
  }

  public get selectedEmp(): Employees {
    return this._selectedEmp;
  }
  public set selectedEmp(value: Employees) {
    this._selectedEmp = value;
  }

  public get selectedDep(): Departments {
    return this._selectedDep;
  }
  public set selectedDep(value: Departments) {
    this._selectedDep = value;
  }
  public get selectedGroup(): Groups {
    return this._selectedGroup;
  }
  public set selectedGroup(value: Groups) {
    this._selectedGroup = value;
  }
  public get editDep(): boolean {
    return this._editDep;
  }
  public set editDep(value: boolean) {
    this._editDep = value;
  }
  public get editGroup(): boolean {
    return this._editGroup;
  }
  public set editGroup(value: boolean) {
    this._editGroup = value;
  }
  public get employees(): boolean {
    return this._employees;
  }
  public set employees(value: boolean) {
    this._employees = value;
  }
  public get workGroup(): boolean {
    return this._workGroup;
  }
  public set workGroup(value: boolean) {
    this._workGroup = value;
  }
  public get department(): boolean {
    return this._department;
  }
  public set department(value: boolean) {
    this._department = value;
  }


  ShowEmployees() {

   
    let c = document.getElementById("emp");
    let b = document.getElementById("dep");
    let a = document.getElementById("work");

    c.style.backgroundColor = "#C1CDD4";
    b.style.backgroundColor = "#FAFAFA";
    a.style.backgroundColor = "#FAFAFA";

    this._employees = true;
    this._department = false;
    this._workGroup = false;

    this._appService.TransactionService.GetDepartments();
    this._appService.TransactionService.GetGroup();
    this._appService.TransactionService.GetEmployees();
  }

  ShowDep() {

    let c = document.getElementById("emp");
    let b = document.getElementById("dep");
    let a = document.getElementById("work");

    c.style.backgroundColor = "#FAFAFA";
    b.style.backgroundColor = "#C1CDD4";
    a.style.backgroundColor = "#FAFAFA";

    this._employees = false;
    this._department = true;
    this._workGroup = false;

    this._appService.TransactionService.GetDepartments();
  }

  ShowWork() {

    let c = document.getElementById("emp");
    let b = document.getElementById("dep");
    let a = document.getElementById("work");

    c.style.backgroundColor = "#FAFAFA";
    b.style.backgroundColor = "#FAFAFA";
    a.style.backgroundColor = "#C1CDD4";

    this._employees = false;
    this._department = false;
    this._workGroup = true;

    this._appService.TransactionService.GetGroup();
  }

  EditDepartment(data: Departments) {
    this.editDep = true;
    this.selectedDep = new Departments();
    this.selectedDep = data;
    this.depForm.reset();
    this.depForm.controls['description'].setValue(this.selectedDep.desc);
  }

  UpdateDepartment() {
    
    let c: Departments = new Departments();
    c.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    c.desc = this.depForm.get('description').value;
    c.id = this.selectedDep.id;
    this._appService.TransactionService.UpdateDepartment(c);
    this.editDep = false;
  }

  UpdateGroup() {
    let c: Departments = new Departments();
    c.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    c.desc = this.groupForm.get('description').value;
    c.id = this.selectedGroup.id;
    this._appService.TransactionService.UpdateGroup(c);
    this._editGroup = false;
  }

  EditEmployee(data: Employees) {
    this._selectedEmp = new Employees();

    this._selectedEmp = data;

    this.empForm.reset();

    this.empForm.controls['name'].setValue(data.name);
    this.empForm.controls['surname'].setValue(data.surname);
    this.empForm.controls['cell'].setValue(data.cell);
    this.empForm.controls['email'].setValue(data.email);
    this.empForm.controls['dep'].setValue(data.dep);
    this.empForm.controls['grp'].setValue(data.group);
    this.empForm.controls['laptop'].setValue(data.laptop);
    this.empForm.controls['wifi'].setValue(data.wifi);
    this.empForm.controls['uncaped'].setValue(data.uncapped);

    this._editEmp = true;
  }

  DeleteEmployee(data: Employees) {
    let c = <HTMLInputElement> document.getElementById("empDelete");

    data.deleted = c.checked;
    data.changeby = this._appService.LoginService.CurrentLoggedInUser.username;
    this._appService.TransactionService.DeleteEmployees(data);
  }

  UpdateEmployee() {

    let c = new Employees();
    c.name = this.empForm.get('name').value;
    c.surname = this.empForm.get('surname').value;
    c.cell = this.empForm.get('cell').value;
    c.email = this.empForm.get('email').value;
    c.dep = this.empForm.get('dep').value;
    c.group = this.empForm.get('grp').value;
    c.laptop = this.empForm.get('laptop').value;
    c.wifi = this.empForm.get('wifi').value;
    c.uncapped = this.empForm.get('uncaped').value;
    c.createBy = this._appService.LoginService.CurrentLoggedInUser.username;
    c.changeby = this._appService.LoginService.CurrentLoggedInUser.username;
    c.id = this.selectedEmp.id;


    this._appService.TransactionService.UpdateEmployees(c);

    this.empForm.reset();
    this._editEmp = false;
  }

  DeleteDepartment(data: Departments) {
    let c = <HTMLInputElement>document.getElementById("depDelete");

    data.deleted = c.checked;
    data.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    this._appService.TransactionService.DeleteDepartMent(data);

  }

  EditWorkGroup(data: Groups) {
    this.editGroup = true;
    this.selectedGroup = new Groups();
    this.selectedGroup = data;
    this.groupForm.reset();
    this.groupForm.controls['description'].setValue(this.selectedGroup.desc);
  }

  DeleteGroup(data: Groups) {
    let c = <HTMLInputElement>document.getElementById("grpDelete");

    data.deleted = c.checked;
    data.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    this._appService.TransactionService.DeleteGroupt(data);
  }

  NewEmployee() {

    let c = new Employees();
    c.name = this.empForm.get('name').value;
    c.surname = this.empForm.get('surname').value;
    c.cell = this.empForm.get('cell').value;
    if (c.cell == null || c.cell == undefined || c.cell == "") {
      c.cell = " ";
    }
    c.email = this.empForm.get('email').value;
    if (c.email == null || c.email == undefined || c.email == "") {
      c.email = " ";
    }
    c.dep = this.empForm.get('dep').value;
    c.group = this.empForm.get('grp').value;
    c.laptop = this.empForm.get('laptop').value;
    if (c.laptop == null || c.laptop == undefined) {
      c.laptop = false; 
    }
    c.wifi = this.empForm.get('wifi').value;
    if (c.wifi == null || c.wifi == undefined) {
      c.wifi = false;
    }
    c.uncapped = this.empForm.get('uncaped').value;
    if (c.uncapped == null || c.uncapped == undefined) {
      c.uncapped = false;
    }
    c.createBy = this._appService.LoginService.CurrentLoggedInUser.username;
    c.changeby = this._appService.LoginService.CurrentLoggedInUser.username;



    this._appService.TransactionService.NewEmployee(c);

    this.empForm.reset();
    this._newEmp = false;
  }

  NewDepartment() {
    let c: Departments = new Departments();
    c.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    c.createBy = this._appService.LoginService.CurrentLoggedInUser.username;
    c.desc = this.depForm.get('description').value;

    this._appService.TransactionService.NewDepartment(c);

    this._newDep = false;
  }

  NewGroup() {
    let c: Groups = new Groups();
    c.changeBy = this._appService.LoginService.CurrentLoggedInUser.username;
    c.createBy = this._appService.LoginService.CurrentLoggedInUser.username;
    c.desc = this.groupForm.get('description').value;
    this._appService.TransactionService.NewGroup(c);
    this._newGroup = false;
  }

  OpenNewGroup() {
    this.groupForm.reset();
    this._newGroup = true;
  }
  OpenNewEmployee() {
    this.empForm.reset();
    this._newEmp = true;
  }
  OpenNewDep() {
    this.depForm.reset();
    this._newDep = true;
  }

  SortLaptopField() {
    if (this._lapCount < 1) {
      this._appService.TransactionService.employeeList = this._appService.TransactionService.employeeList.sort((a, b) => a.laptop > b.laptop ? -1 : 1);
      this._lapCount++;
    } else {
      this._appService.TransactionService.employeeList = this._appService.TransactionService.employeeList.sort((a, b) => b.laptop > a.laptop ? -1 : 1);
      this._lapCount = 0
    }
  }
  SortWifiField() {
    if (this._wifiCount < 1) {
      this._appService.TransactionService.employeeList = this._appService.TransactionService.employeeList.sort((a, b) => a.wifi > b.wifi ? -1 : 1);
      this._wifiCount++;
    } else {
      this._appService.TransactionService.employeeList = this._appService.TransactionService.employeeList.sort((a, b) => b.wifi > a.wifi ? -1 : 1);
      this._wifiCount = 0
    }
  }
  SortUncapField() {
    if (this._uncapCount < 1) {
      this._appService.TransactionService.employeeList = this._appService.TransactionService.employeeList.sort((a, b) => a.uncapped > b.uncapped ? -1 : 1);
      this._uncapCount++;
    } else {
      this._appService.TransactionService.employeeList = this._appService.TransactionService.employeeList.sort((a, b) => b.uncapped > a.uncapped ? -1 : 1);
      this._uncapCount = 0
    }
  }

}
