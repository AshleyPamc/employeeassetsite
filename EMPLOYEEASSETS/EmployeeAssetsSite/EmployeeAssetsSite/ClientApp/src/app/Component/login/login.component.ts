import { Component, OnInit, Input } from '@angular/core';
import { AppService } from '../../Service/app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() loginForm: FormGroup;

  constructor(private _appService: AppService, private fb: FormBuilder) { }

  ngOnInit() {
    this._appService.LoginService.ClearLoginFields();
    this.loginForm = this.fb.group({
      'username': ['', Validators.required],
      'password': ['', Validators.required]
    })
  }

  Login() {
    let c: string;
    let d: string;

    c = this.loginForm.get('username').value;
    d = this.loginForm.get('password').value;

    this._appService.LoginService.Username = c;
    this._appService.LoginService.Password = d;

    this._appService.LoginService.Login();
    setTimeout((a) => {
      this._appService.TransactionService.GetDashView();
    }, 1000)


  }
}
