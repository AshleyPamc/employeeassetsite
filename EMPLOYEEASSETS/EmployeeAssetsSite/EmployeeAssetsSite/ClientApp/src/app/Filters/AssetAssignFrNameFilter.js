"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AssetAssignFrNameFilter = /** @class */ (function () {
    function AssetAssignFrNameFilter() {
    }
    AssetAssignFrNameFilter.prototype.accepts = function (i, search) {
        return "" + i.frName == search
            || i.frName.toLowerCase().indexOf(search) >= 0;
    };
    return AssetAssignFrNameFilter;
}());
exports.AssetAssignFrNameFilter = AssetAssignFrNameFilter;
//# sourceMappingURL=AssetAssignFrNameFilter.js.map