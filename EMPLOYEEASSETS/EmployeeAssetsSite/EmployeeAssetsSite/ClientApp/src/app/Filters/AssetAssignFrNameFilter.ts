import { AssetAssign } from "../Models/asset-assign";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class AssetAssignFrNameFilter implements ClrDatagridStringFilterInterface<AssetAssign> {
  accepts(i: AssetAssign, search: string): boolean {
    return "" + i.frName == search
      || i.frName.toLowerCase().indexOf(search) >= 0;
  }
}
