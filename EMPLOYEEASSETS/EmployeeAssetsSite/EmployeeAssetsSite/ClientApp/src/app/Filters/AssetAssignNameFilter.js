"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AssetAssignNameFilter = /** @class */ (function () {
    function AssetAssignNameFilter() {
    }
    AssetAssignNameFilter.prototype.accepts = function (i, search) {
        return "" + i.empName == search
            || i.empName.toLowerCase().indexOf(search) >= 0;
    };
    return AssetAssignNameFilter;
}());
exports.AssetAssignNameFilter = AssetAssignNameFilter;
//# sourceMappingURL=AssetAssignNameFilter.js.map