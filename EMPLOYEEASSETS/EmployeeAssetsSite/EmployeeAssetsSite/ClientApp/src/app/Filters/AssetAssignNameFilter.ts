import { AssetAssign } from "../Models/asset-assign";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class AssetAssignNameFilter implements ClrDatagridStringFilterInterface<AssetAssign> {
  accepts(i: AssetAssign, search: string): boolean {
    return "" + i.empName == search
      || i.empName.toLowerCase().indexOf(search) >= 0;
  }
}
