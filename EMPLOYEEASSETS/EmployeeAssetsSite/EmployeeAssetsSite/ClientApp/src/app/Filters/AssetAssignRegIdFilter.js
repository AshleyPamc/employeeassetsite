"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AssetAssignRegIdFilter = /** @class */ (function () {
    function AssetAssignRegIdFilter() {
    }
    AssetAssignRegIdFilter.prototype.accepts = function (i, search) {
        return "" + i.assetRegId == search
            || i.assetRegId.toLowerCase().indexOf(search) >= 0;
    };
    return AssetAssignRegIdFilter;
}());
exports.AssetAssignRegIdFilter = AssetAssignRegIdFilter;
//# sourceMappingURL=AssetAssignRegIdFilter.js.map