import { AssetAssign } from "../Models/asset-assign";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class AssetAssignRegIdFilter implements ClrDatagridStringFilterInterface<AssetAssign> {
  accepts(i: AssetAssign, search: string): boolean {
    return "" + i.assetRegId == search
      || i.assetRegId.toLowerCase().indexOf(search) >= 0;
  }
}
