"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AssetAssignTypeFilter = /** @class */ (function () {
    function AssetAssignTypeFilter() {
    }
    AssetAssignTypeFilter.prototype.accepts = function (i, search) {
        return "" + i.assetName == search
            || i.assetName.toLowerCase().indexOf(search) >= 0;
    };
    return AssetAssignTypeFilter;
}());
exports.AssetAssignTypeFilter = AssetAssignTypeFilter;
//# sourceMappingURL=AssetAssignTypeFilter.js.map