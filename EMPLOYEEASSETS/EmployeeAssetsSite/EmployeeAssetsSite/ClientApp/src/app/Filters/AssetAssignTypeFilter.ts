import { AssetAssign } from "../Models/asset-assign";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class AssetAssignTypeFilter implements ClrDatagridStringFilterInterface<AssetAssign> {
  accepts(i: AssetAssign, search: string): boolean {
    return "" + i.assetName == search
      || i.assetName.toLowerCase().indexOf(search) >= 0;
  }
}
