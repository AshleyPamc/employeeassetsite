"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AssetCellFilter = /** @class */ (function () {
    function AssetCellFilter() {
    }
    AssetCellFilter.prototype.accepts = function (i, search) {
        return "" + i.cell == search
            || i.cell.toLowerCase().indexOf(search) >= 0;
    };
    return AssetCellFilter;
}());
exports.AssetCellFilter = AssetCellFilter;
//# sourceMappingURL=AssetCellFilter.js.map