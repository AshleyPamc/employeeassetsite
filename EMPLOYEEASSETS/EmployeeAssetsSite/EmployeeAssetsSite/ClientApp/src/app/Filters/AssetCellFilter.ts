import { Assets } from "../Models/assets";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class AssetCellFilter implements ClrDatagridStringFilterInterface<Assets> {
  accepts(i: Assets, search: string): boolean {
    return "" + i.cell == search
      || i.cell.toLowerCase().indexOf(search) >= 0;
  }
}
