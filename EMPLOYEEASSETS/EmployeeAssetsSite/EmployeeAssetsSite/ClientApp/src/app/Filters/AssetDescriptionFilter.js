"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AssetDescriptionFilter = /** @class */ (function () {
    function AssetDescriptionFilter() {
    }
    AssetDescriptionFilter.prototype.accepts = function (i, search) {
        return "" + i.desc == search
            || i.desc.toLowerCase().indexOf(search) >= 0;
    };
    return AssetDescriptionFilter;
}());
exports.AssetDescriptionFilter = AssetDescriptionFilter;
//# sourceMappingURL=AssetDescriptionFilter.js.map