import { Assets } from "../Models/assets";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class AssetDescriptionFilter implements ClrDatagridStringFilterInterface<Assets> {
  accepts(i: Assets, search: string): boolean {
    return "" + i.desc == search
      || i.desc.toLowerCase().indexOf(search) >= 0;
  }
}
