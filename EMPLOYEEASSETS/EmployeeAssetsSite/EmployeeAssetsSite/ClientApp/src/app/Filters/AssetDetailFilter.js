"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AssetDetailFilter = /** @class */ (function () {
    function AssetDetailFilter() {
    }
    AssetDetailFilter.prototype.accepts = function (i, search) {
        return "" + i.desc == search
            || i.desc.toLowerCase().indexOf(search) >= 0;
    };
    return AssetDetailFilter;
}());
exports.AssetDetailFilter = AssetDetailFilter;
//# sourceMappingURL=AssetDetailFilter.js.map