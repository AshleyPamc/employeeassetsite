import { ClrDatagridStringFilterInterface } from "@clr/angular";
import { AssetDetails } from "../Models/asset-details";

export class AssetDetailFilter implements ClrDatagridStringFilterInterface<AssetDetails> {
  accepts(i: AssetDetails, search: string): boolean {
    return "" + i.desc == search
      || i.desc.toLowerCase().indexOf(search) >= 0;
  }
}
