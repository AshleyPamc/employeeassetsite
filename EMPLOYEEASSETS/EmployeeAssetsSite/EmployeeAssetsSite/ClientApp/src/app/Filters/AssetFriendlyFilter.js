"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AssetFriendlyFilter = /** @class */ (function () {
    function AssetFriendlyFilter() {
    }
    AssetFriendlyFilter.prototype.accepts = function (i, search) {
        return "" + i.frName == search
            || i.frName.toLowerCase().indexOf(search) >= 0;
    };
    return AssetFriendlyFilter;
}());
exports.AssetFriendlyFilter = AssetFriendlyFilter;
//# sourceMappingURL=AssetFriendlyFilter.js.map