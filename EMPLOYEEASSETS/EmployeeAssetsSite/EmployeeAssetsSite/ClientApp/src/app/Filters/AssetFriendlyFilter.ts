import { Assets } from "../Models/assets";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class AssetFriendlyFilter implements ClrDatagridStringFilterInterface<Assets> {
  accepts(i: Assets, search: string): boolean {
    return "" + i.frName == search
      || i.frName.toLowerCase().indexOf(search) >= 0;
  }
}
