"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AssetRegIdFilter = /** @class */ (function () {
    function AssetRegIdFilter() {
    }
    AssetRegIdFilter.prototype.accepts = function (i, search) {
        return "" + i.regId == search
            || i.regId.toLowerCase().indexOf(search) >= 0;
    };
    return AssetRegIdFilter;
}());
exports.AssetRegIdFilter = AssetRegIdFilter;
//# sourceMappingURL=AssetRegIdFilter.js.map