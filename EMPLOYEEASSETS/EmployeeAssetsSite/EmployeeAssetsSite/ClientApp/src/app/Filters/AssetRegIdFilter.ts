import { Assets } from "../Models/assets";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class AssetRegIdFilter implements ClrDatagridStringFilterInterface<Assets> {
  accepts(i: Assets, search: string): boolean {
    return "" + i.regId == search
      || i.regId.toLowerCase().indexOf(search) >= 0;
  }
}
