"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AssignFromToDateFilter = /** @class */ (function () {
    function AssignFromToDateFilter() {
    }
    AssignFromToDateFilter.prototype.accepts = function (i, search) {
        return "" + i.fromDate == search
            || i.fromDate.toLowerCase().indexOf(search) >= 0 || i.toDate == search
            || i.toDate.toLowerCase().indexOf(search) >= 0;
    };
    return AssignFromToDateFilter;
}());
exports.AssignFromToDateFilter = AssignFromToDateFilter;
//# sourceMappingURL=AssignFromToDateFilter.js.map