import { AssetAssign } from "../Models/asset-assign";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class AssignFromToDateFilter implements ClrDatagridStringFilterInterface<AssetAssign> {
  accepts(i: AssetAssign, search: string): boolean {
    return "" + i.fromDate == search
      || i.fromDate.toLowerCase().indexOf(search) >= 0 || i.toDate == search
      || i.toDate.toLowerCase().indexOf(search) >= 0;
  }
}
