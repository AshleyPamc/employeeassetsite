"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DashAssetFilter = /** @class */ (function () {
    function DashAssetFilter() {
    }
    DashAssetFilter.prototype.accepts = function (i, search) {
        return "" + i.assetType == search
            || i.assetType.toLowerCase().indexOf(search) >= 0;
    };
    return DashAssetFilter;
}());
exports.DashAssetFilter = DashAssetFilter;
//# sourceMappingURL=DashAssetFilter.js.map