"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DashCellFilter = /** @class */ (function () {
    function DashCellFilter() {
    }
    DashCellFilter.prototype.accepts = function (i, search) {
        return "" + i.cell == search
            || i.cell.toLowerCase().indexOf(search) >= 0;
    };
    return DashCellFilter;
}());
exports.DashCellFilter = DashCellFilter;
//# sourceMappingURL=DashCellFilter.js.map