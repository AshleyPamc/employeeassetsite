"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DashDateFilter = /** @class */ (function () {
    function DashDateFilter() {
    }
    DashDateFilter.prototype.accepts = function (i, search) {
        return "" + i.fromDate == search
            || i.fromDate.toLowerCase().indexOf(search) >= 0 || "" + i.toDate == search
            || i.toDate.toLowerCase().indexOf(search) >= 0;
    };
    return DashDateFilter;
}());
exports.DashDateFilter = DashDateFilter;
//# sourceMappingURL=DashDateFilter.js.map