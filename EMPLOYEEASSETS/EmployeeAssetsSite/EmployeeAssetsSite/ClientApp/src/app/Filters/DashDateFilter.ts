import { ExcelListModel } from "../Models/excel-list-model";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class DashDateFilter implements ClrDatagridStringFilterInterface<ExcelListModel> {
  accepts(i: ExcelListModel, search: string): boolean {
    return "" + i.fromDate == search
      || i.fromDate.toLowerCase().indexOf(search) >= 0 || "" + i.toDate == search
      || i.toDate.toLowerCase().indexOf(search) >= 0;
  }
}
