"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DashDepFilter = /** @class */ (function () {
    function DashDepFilter() {
    }
    DashDepFilter.prototype.accepts = function (i, search) {
        return "" + i.department == search
            || i.department.toLowerCase().indexOf(search) >= 0;
    };
    return DashDepFilter;
}());
exports.DashDepFilter = DashDepFilter;
//# sourceMappingURL=DashDepFilter.js.map