"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DashFrNameFilter = /** @class */ (function () {
    function DashFrNameFilter() {
    }
    DashFrNameFilter.prototype.accepts = function (i, search) {
        return "" + i.frName == search
            || i.frName.toLowerCase().indexOf(search) >= 0;
    };
    return DashFrNameFilter;
}());
exports.DashFrNameFilter = DashFrNameFilter;
//# sourceMappingURL=DashFrNameFilter.js.map