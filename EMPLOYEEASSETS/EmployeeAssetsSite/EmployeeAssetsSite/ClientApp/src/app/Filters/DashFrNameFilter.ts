import { ExcelListModel } from "../Models/excel-list-model";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class DashFrNameFilter implements ClrDatagridStringFilterInterface<ExcelListModel> {
  accepts(i: ExcelListModel, search: string): boolean {
    return "" + i.frName == search
      || i.frName.toLowerCase().indexOf(search) >= 0;
  }
}
