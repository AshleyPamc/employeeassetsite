"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DashNameFilter = /** @class */ (function () {
    function DashNameFilter() {
    }
    DashNameFilter.prototype.accepts = function (i, search) {
        return "" + i.name == search
            || i.name.toLowerCase().indexOf(search) >= 0 || "" + i.surname == search
            || i.surname.toLowerCase().indexOf(search) >= 0;
    };
    return DashNameFilter;
}());
exports.DashNameFilter = DashNameFilter;
//# sourceMappingURL=DashNameFilter.js.map