import { ExcelListModel } from "../Models/excel-list-model";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class DashNameFilter implements ClrDatagridStringFilterInterface<ExcelListModel> {
  accepts(i: ExcelListModel, search: string): boolean {
    return "" + i.name == search
      || i.name.toLowerCase().indexOf(search) >= 0 || "" + i.surname == search
      || i.surname.toLowerCase().indexOf(search) >= 0;
  }
}
