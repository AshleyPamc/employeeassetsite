"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DashRegIdFilter = /** @class */ (function () {
    function DashRegIdFilter() {
    }
    DashRegIdFilter.prototype.accepts = function (i, search) {
        return "" + i.regId == search
            || i.regId.toLowerCase().indexOf(search) >= 0;
    };
    return DashRegIdFilter;
}());
exports.DashRegIdFilter = DashRegIdFilter;
//# sourceMappingURL=DashRegIdFilter.js.map