import { ExcelListModel } from "../Models/excel-list-model";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class DashRegIdFilter implements ClrDatagridStringFilterInterface<ExcelListModel> {
  accepts(i: ExcelListModel, search: string): boolean {
    return "" + i.regId == search
      || i.regId.toLowerCase().indexOf(search) >= 0;
  }
}
