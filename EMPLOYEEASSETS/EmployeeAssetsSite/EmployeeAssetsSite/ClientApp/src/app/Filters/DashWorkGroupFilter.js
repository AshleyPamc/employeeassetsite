"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DashWorkGroupFilter = /** @class */ (function () {
    function DashWorkGroupFilter() {
    }
    DashWorkGroupFilter.prototype.accepts = function (i, search) {
        return "" + i.group == search
            || i.group.toLowerCase().indexOf(search) >= 0;
    };
    return DashWorkGroupFilter;
}());
exports.DashWorkGroupFilter = DashWorkGroupFilter;
//# sourceMappingURL=DashWorkGroupFilter.js.map