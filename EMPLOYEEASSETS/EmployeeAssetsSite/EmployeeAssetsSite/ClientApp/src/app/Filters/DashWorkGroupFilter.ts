import { ExcelListModel } from "../Models/excel-list-model";
import { ClrDatagridStringFilterInterface } from "@clr/angular";

export class DashWorkGroupFilter implements ClrDatagridStringFilterInterface<ExcelListModel> {
  accepts(i: ExcelListModel, search: string): boolean {
    return "" + i.group == search
      || i.group.toLowerCase().indexOf(search) >= 0 ;
  }
}
