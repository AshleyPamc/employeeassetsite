"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var DepartmentFilter = /** @class */ (function () {
    function DepartmentFilter() {
    }
    DepartmentFilter.prototype.accepts = function (i, search) {
        return "" + i.desc == search
            || i.desc.toLowerCase().indexOf(search) >= 0;
    };
    return DepartmentFilter;
}());
exports.DepartmentFilter = DepartmentFilter;
//# sourceMappingURL=DepartmentFilter.js.map