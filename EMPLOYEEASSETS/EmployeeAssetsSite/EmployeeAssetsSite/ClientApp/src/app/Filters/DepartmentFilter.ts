import { ClrDatagridStringFilterInterface } from "@clr/angular";
import { Departments } from "../Models/departments";

export class DepartmentFilter implements ClrDatagridStringFilterInterface<Departments> {
  accepts(i: Departments, search: string): boolean {
    return "" + i.desc == search
      || i.desc.toLowerCase().indexOf(search) >= 0;
  }
}
