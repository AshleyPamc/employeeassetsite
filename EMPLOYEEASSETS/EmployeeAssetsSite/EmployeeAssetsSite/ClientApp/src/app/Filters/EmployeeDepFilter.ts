import { ClrDatagridStringFilterInterface } from "@clr/angular";
import { Employees } from "../Models/employees";

export class EmployeeDepFilter implements ClrDatagridStringFilterInterface<Employees> {
  accepts(user: Employees, search: string): boolean {
    return "" + user.depDesc == search
      || user.depDesc.toLowerCase().indexOf(search) >= 0;
  }
}
