"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EmployeeNameFilter = /** @class */ (function () {
    function EmployeeNameFilter() {
    }
    EmployeeNameFilter.prototype.accepts = function (user, search) {
        return "" + user.name == search
            || user.name.toLowerCase().indexOf(search) >= 0 || user.surname == search || user.surname.toLowerCase().indexOf(search) >= 0;
    };
    return EmployeeNameFilter;
}());
exports.EmployeeNameFilter = EmployeeNameFilter;
//# sourceMappingURL=EmployeeNameFilter.js.map