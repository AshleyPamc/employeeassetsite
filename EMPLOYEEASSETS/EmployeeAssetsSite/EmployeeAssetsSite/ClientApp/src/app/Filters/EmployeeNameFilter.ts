import { ClrDatagridStringFilterInterface } from "@clr/angular";
import { Employees } from "../Models/employees";

export class EmployeeNameFilter implements ClrDatagridStringFilterInterface<Employees> {
  accepts(user: Employees, search: string): boolean {
    return "" + user.name == search
      || user.name.toLowerCase().indexOf(search) >= 0 || user.surname == search || user.surname.toLowerCase().indexOf(search) >= 0;
  }
}
