"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var EmployeeSurnameFilter = /** @class */ (function () {
    function EmployeeSurnameFilter() {
    }
    EmployeeSurnameFilter.prototype.accepts = function (user, search) {
        return "" + user.surname == search
            || user.surname.toLowerCase().indexOf(search) >= 0 || user.name == search
            || user.name.toLowerCase().indexOf(search) >= 0;
    };
    return EmployeeSurnameFilter;
}());
exports.EmployeeSurnameFilter = EmployeeSurnameFilter;
//# sourceMappingURL=EmployeeSurnameFilter.js.map