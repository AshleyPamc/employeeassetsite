import { ClrDatagridStringFilterInterface } from "@clr/angular";
import { Employees } from "../Models/employees";

export class EmployeeSurnameFilter implements ClrDatagridStringFilterInterface<Employees> {
  accepts(user: Employees, search: string): boolean {
    return "" + user.surname == search
      || user.surname.toLowerCase().indexOf(search) >= 0|| user.name == search
        || user.name.toLowerCase().indexOf(search) >= 0;
  }
}
