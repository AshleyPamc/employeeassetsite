import { ClrDatagridStringFilterInterface } from "@clr/angular";
import { Employees } from "../Models/employees";

export class EmployeeWorkGroupFilter implements ClrDatagridStringFilterInterface<Employees> {
  accepts(user: Employees, search: string): boolean {
    return "" + user.groupDesc == search
      || user.groupDesc.toLowerCase().indexOf(search) >= 0;
  }
}
