"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var WorkGroupFilter = /** @class */ (function () {
    function WorkGroupFilter() {
    }
    WorkGroupFilter.prototype.accepts = function (i, search) {
        return "" + i.desc == search
            || i.desc.toLowerCase().indexOf(search) >= 0;
    };
    return WorkGroupFilter;
}());
exports.WorkGroupFilter = WorkGroupFilter;
//# sourceMappingURL=WorkGroupFilter.js.map