import { ClrDatagridStringFilterInterface } from "@clr/angular";
import { Groups } from "../Models/groups";

export class WorkGroupFilter implements ClrDatagridStringFilterInterface<Groups> {
  accepts(i: Groups, search: string): boolean {
    return "" + i.desc == search
      || i.desc.toLowerCase().indexOf(search) >= 0;
  }
}
