export class AssetAssign {
  public id: string;
  public empId: string;
  public empName: string;
  public frName: string;
  public assetId: string;
  public assetRegId: string;
  public assetName: string;
  public fromDate: string;
  public toDate: string;
  public createDate: string;
  public changeDate: string;
  public createBy: string;
  public changeBy: string;
}
