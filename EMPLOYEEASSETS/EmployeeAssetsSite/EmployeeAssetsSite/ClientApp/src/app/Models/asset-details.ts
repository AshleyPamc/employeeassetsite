export class AssetDetails {
  public type: string;
  public desc: string;
  public createBy: string;
  public createDate: string;
  public changeBy: string;
  public changeDate: string;
  public deleted: boolean;
}
