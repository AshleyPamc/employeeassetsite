export class Assets {
  public id: string;
  public regId: string;
  public type: string;
  public desc: string;
  public cell: string;
  public createBy: string;
  public createDate: string;
  public changeDate: string;
  public changeBy: string;
  public deleted: boolean;
  public frName: string;
  public selected: boolean;
  public assigned: boolean;
}
