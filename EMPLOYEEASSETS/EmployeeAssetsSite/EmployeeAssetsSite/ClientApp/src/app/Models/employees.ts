export class Employees {
  public id: string;
  public name: string;
  public surname: string;
  public cell: string;
  public email: string;
  public dep: string;
  public depDesc: string;
  public group: string;
  public groupDesc: string;
  public laptop: boolean;
  public wifi: boolean;
  public uncapped: boolean;
  public createDate: string;
  public createBy: string;
  public changeDate: string;
  public changeby: string;
  public deleted: boolean;
  public selected: boolean;
  public assigned: boolean;
}
