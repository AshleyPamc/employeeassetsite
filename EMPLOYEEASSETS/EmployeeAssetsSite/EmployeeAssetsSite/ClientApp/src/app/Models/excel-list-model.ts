export class ExcelListModel {

  public name: string;
  public surname: string;
  public department: string;
  public laptop: boolean;
  public wifi: boolean;
  public uncaped: boolean;
  public group: string;
  public assetType: string;
  public regId: string;
  public frName: string;
  public cell: string;
  public fromDate: string;
  public toDate: string;
  public assign: boolean;
  public pamcLaptop: boolean;
  public pamcDongle: boolean;
  public pamcScreen: boolean;
  public emId: string;
}
