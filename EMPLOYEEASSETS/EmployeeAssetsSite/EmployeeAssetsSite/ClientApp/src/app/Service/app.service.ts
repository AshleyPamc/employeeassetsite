import { Injectable } from '@angular/core';
import { LoginService } from './login.service';
import { TransactionService } from './transaction.service';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private _loginService: LoginService, private _transactionService: TransactionService) { }

  public get LoginService(): LoginService{
    return this._loginService;
  }

  public get TransactionService(): TransactionService {
    return this._transactionService;
  }

}
