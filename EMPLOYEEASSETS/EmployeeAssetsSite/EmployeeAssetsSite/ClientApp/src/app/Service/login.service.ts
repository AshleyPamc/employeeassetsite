import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { ValidationResultsModel } from '../Models/ValidationResultsModel';
import { LoginDetailsModel } from '../Models/LoginDetailsModel';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private _webAPIavailable: boolean = false;
  private _server: string;
  private _busy: boolean;
  private _subscribeError: boolean = false;
  private show_hide_error: boolean = false;
  private _token: string;
  private _userName: string;
  private _password: string;
  private _isLoggedin: boolean = false;
  private _loggedInuser: LoginDetailsModel;





  constructor(private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string, private _router: Router) {
    this.TestConnection();
  }

  public get SQLConnectionStatus() {
    return this._webAPIavailable
  }
  public get server(): string {
    return this._server;
  }
  public set server(value: string) {
    this._server = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get ErrorOccurance() {
    return this._subscribeError
  }
  public get token(): string {
    return this._token;
  }
  public set token(value: string) {
    this._token = value;
  }
  public get Username() {
    return this._userName
  }
  public set Username(name) {
    this._userName = name
  }
  public get Password() {
    return this._password
  }
  public set Password(pass) {
    this._password = pass
  }
  public get IsLoggedIn(): boolean {
    return this._isLoggedin;
  }
  public set IsLoggedIn(value: boolean) {
    this._isLoggedin = value;
  }
  public get CurrentLoggedInUser() {
    return this._loggedInuser
  }



  TestConnection() {
    this._http.get(this._baseUrl + "api/Login/validConnection").subscribe(
      (connectionStatus: ValidationResultsModel) => {
        this._webAPIavailable = connectionStatus.valid;
        if (this._webAPIavailable == true) {
          this._server = connectionStatus.message;
          //this.LoadMemberRegistrationHealthPlans();
        }
      }, (error) => {
        this._subscribeError = true
        this._busy = false;
      },
      () => {

      }

    )




  }

  Login() {
    this._busy = true
    this._http
      .post(this._baseUrl + "api/Login/LoginDetails", {
        Username: this.Username,
        Password: this.Password
      }).subscribe((webresult: LoginDetailsModel) => {
        if (webresult.success == false) {
          this.show_hide_error = true
          this._userName = "";
          this._password = "";
          this._isLoggedin = false;
          this._loggedInuser = new LoginDetailsModel();
        } else {
          this._loggedInuser = webresult;
          this.IsLoggedIn = true;

              this._router.navigate(["dashboard"])
              this._loggedInuser.username = webresult.username;
              this._loggedInuser.firstname = webresult.firstname;
              this._loggedInuser.lastname = webresult.lastname;
        }

      }, (error) => {
        //todo: handle error propperly. What will the user see if this error fires???
        console.log(error);

      },
        () => {
          if (this._loggedInuser.success === true) {
            this.GetTokenForUser();
          }
          this._busy = false

        });

  }

  GetTokenForUser() {
    this._busy = true;
    this._http.post(this._baseUrl + 'api/Token/token', this.CurrentLoggedInUser, { responseType: "text" })
      .subscribe((results: string) => {
        let jwt: string;
        jwt = results;
        localStorage.setItem("jwt", jwt);
        this.token = results;
      },
        (error) => {
          console.log(error);
        },
        () => {
          this._token = localStorage.getItem("jwt");
          this._busy = false;
        })
  }

  ClearLoginFields() {
    this._userName = "";
    this._password = "";
  }
}
