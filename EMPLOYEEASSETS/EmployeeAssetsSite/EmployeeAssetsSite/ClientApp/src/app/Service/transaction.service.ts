import { Injectable, Inject } from '@angular/core';
import { Groups } from '../Models/groups';
import { Departments } from '../Models/departments';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { ValidationResultsModel } from '../Models/ValidationResultsModel';
import { Employees } from '../Models/employees';
import { Assets } from '../Models/assets';
import { AssetDetails } from '../Models/asset-details';
import { AssetAssign } from '../Models/asset-assign';
import { ExcelListModel } from '../Models/excel-list-model';
import { AssetCount } from '../Models/asset-count';
import { Files } from '../Models/files';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  private _assetHist: AssetAssign[] = [];
  private _workGroupList: Groups[] = [];
  private _departmentList: Departments[] = [];
  private _employeeList: Employees[] = [];
  private _busy: boolean = false;
  private _error: boolean = true;
  private _errMsg: string = "";
  private _updateSuccess: boolean = false;
  private _deleteSuccess: boolean = false;
  private _insertSuccess: boolean = false;
  private _listAssets: Assets[] = [];
  private _listAssetDetail: AssetDetails[] = [];
  private _assetAssignList: AssetAssign[] = [];
  private _unassignedAssets: Assets[] = [];
  private _unassignedEmp: Employees[] = [];
  private _dashList: ExcelListModel[] = [];
  private _unassignedList: AssetCount[] = [];


  constructor(private _http: HttpClient, @Inject('BASE_URL') public _baseUrl: string, private _router: Router) { }

  public get assetHist(): AssetAssign[] {
    return this._assetHist;
  }
  public set assetHist(value: AssetAssign[]) {
    this._assetHist = value;
  }
  public get unassignedList(): AssetCount[] {
    return this._unassignedList;
  }
  public set unassignedList(value: AssetCount[]) {
    this._unassignedList = value;
  }
  public get unassignedAssets(): Assets[] {
    return this._unassignedAssets;
  }
  public set unassignedAssets(value: Assets[]) {
    this._unassignedAssets = value;
  }
  public get assetAssignList(): AssetAssign[] {
    return this._assetAssignList;
  }
  public set assetAssignList(value: AssetAssign[]) {
    this._assetAssignList = value;
  }
  public get listAssets(): Assets[] {
    return this._listAssets;
  }
  public set listAssets(value: Assets[]) {
    this._listAssets = value;
  }
  public get listAssetDetail(): AssetDetails[] {
    return this._listAssetDetail;
  }
  public set listAssetDetail(value: AssetDetails[]) {
    this._listAssetDetail = value;
  }
  public get deleteSuccess(): boolean {
    return this._deleteSuccess;
  }
  public set deleteSuccess(value: boolean) {
    this._deleteSuccess = value;
  }
  public get updateSuccess(): boolean {
    return this._updateSuccess;
  }
  public set updateSuccess(value: boolean) {
    this._updateSuccess = value;
  }
  public get errMsg(): string {
    return this._errMsg;
  }
  public set errMsg(value: string) {
    this._errMsg = value;
  }
  public get error(): boolean {
    return this._error;
  }
  public set error(value: boolean) {
    this._error = value;
  }
  public get departmentList(): Departments[] {
    return this._departmentList;
  }
  public set departmentList(value: Departments[]) {
    this._departmentList = value;
  }
  public get employeeList(): Employees[] {
    return this._employeeList;
  }
  public set employeeList(value: Employees[]) {
    this._employeeList = value;
  }
  public get workGroupList(): Groups[] {
    return this._workGroupList;
  }
  public set workGroupList(value: Groups[]) {
    this._workGroupList = value;
  }
  public get busy(): boolean {
    return this._busy;
  }
  public set busy(value: boolean) {
    this._busy = value;
  }
  public get insertSuccess(): boolean {
    return this._insertSuccess;
  }
  public set insertSuccess(value: boolean) {
    this._insertSuccess = value;
  }
  public get unassignedEmp(): Employees[] {
    return this._unassignedEmp;
  }
  public set unassignedEmp(value: Employees[]) {
    this._unassignedEmp = value;
  }
  public get dashList(): ExcelListModel[] {
    return this._dashList;
  }
  public set dashList(value: ExcelListModel[]) {
    this._dashList = value;
  }

  GetDepartments() {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.get(this._baseUrl + 'api/Transaction/GetDepartments',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: Departments[]) => {
      this._departmentList = [];
      this.departmentList = results;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
      });

  }

  UpdateDepartment(data: Departments) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/UpdateDepartment', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.updateSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetDepartments();
      });
  }

  DeleteDepartMent(data: Departments) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/DeleteDepartment', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.deleteSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetDepartments();
      });
  }

  NewDepartment(data: Departments) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/NewDepartment', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.insertSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetDepartments();
      });
  }

  GetGroup() {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.get(this._baseUrl + 'api/Transaction/GetWorkGroups',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: Groups[]) => {
      this._workGroupList = [];
      this._workGroupList = results;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
      });

  }

  UpdateGroup(data: Groups) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/UpdateWorkGroup', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.updateSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetGroup();
      });
  }

  DeleteGroupt(data: Groups) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/DeleteWorkGroup', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.deleteSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetGroup();
      });
  }

  NewGroup(data: Groups) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/NewWorkGroup', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.insertSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetGroup();
      });
  }

  GetEmployees() {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.get(this._baseUrl + 'api/Transaction/GetEmployees',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: Employees[]) => {
      this._employeeList = [];
      this._employeeList = results;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
      });

  }

  UpdateEmployees(data: Employees) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/UpdateEmployees', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.updateSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetEmployees();
      });
  }

  DeleteEmployees(data: Employees) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/DeleteEmployees', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.deleteSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetEmployees();
      });
  }

  NewEmployee(data: Employees) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/NewEmployees', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.insertSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetEmployees();
      });
  }

  GetAssets() {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.get(this._baseUrl + 'api/Transaction/GetAssets',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: Assets[]) => {
      this._listAssets = [];
      this._listAssets = results;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
      });

  }

  UpdateAssets(data: Assets) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/UpdateAssets', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.updateSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetAssets();
      });
  }

  DeleteAssets(data: Assets) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/DeleteAssets', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.deleteSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetAssets();
      });
  }

  NewAssets(data: Assets) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/NewAssets', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.insertSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetAssets();
      });
  }

  GetAssetsDetails() {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.get(this._baseUrl + 'api/Transaction/GetAssetsDetails',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: AssetDetails[]) => {
      this._listAssetDetail = [];
      this._listAssetDetail = results;
      this.unassignedList = [];
      this._listAssetDetail.forEach((el) => {
        let c: AssetCount = new AssetCount();
        c.name = el.desc;
        this.unassignedList.push(c);
      });

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
      });

  }

  UpdateAssetsDetails(data: AssetDetails) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/UpdateAssetsDetails', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.updateSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetAssetsDetails();
      });
  }

  DeleteAssetsDetails(data: AssetDetails) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/DeleteAssetsDetails', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.deleteSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetAssetsDetails();
      });
  }

  NewAssetsDetails(data: AssetDetails) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/NewAssetsDetails', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.insertSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetAssetsDetails();
      });
  }

  GetAssetsAssign() {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.get(this._baseUrl + 'api/Transaction/GetAssigments',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: AssetAssign[]) => {
      this._assetAssignList = [];
      this._assetAssignList = results;
      this._assetAssignList = this._assetAssignList.sort((a, b) => a.empName > b.empName ? -1:1);

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
      });

  }

  GetAssetsAssignHist(data: ExcelListModel) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/GetAssigmentsPerEmployee',data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: AssetAssign[]) => {
      this._assetHist = [];
      this._assetHist = results;
      this._assetHist = this._assetHist.sort((a, b) => a.empName > b.empName ? -1 : 1);

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
      });

  }


  UpdateAssetsAssign(data: AssetAssign) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/UpdateAssetsAssing', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.updateSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetAssetsAssign();
      });
  }

  DeleteAssetsAssign(data: AssetAssign) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/DeleteAssetsAssign', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.deleteSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetAssetsAssign();
      });
  }

  NewAssetsAssign(data: AssetAssign) {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.post(this._baseUrl + 'api/Transaction/NewAssetsAssing', data,
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ValidationResultsModel) => {
      this.insertSuccess = results.valid;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetAssetsAssign();
      });
  }

  GetUnAssignedAssets() {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.get(this._baseUrl + 'api/Transaction/GetUnAssignedAssets',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: Assets[]) => {
      this._unassignedAssets = [];
      this._unassignedAssets = results;
      this.unassignedAssets = this.unassignedAssets.sort((a, b) => a.desc > b.desc ? -1 : 1);
      this._unassignedList.forEach((el) => {
        let count: number = 0;
        this.unassignedAssets.forEach((e) => {
          if (el.name.toUpperCase() == e.desc.toUpperCase()) {
            count++;
          }
        });
        el.available = count.toString();
      })

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
      });
  }

  GetUnAssignedEmployess() {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.get(this._baseUrl + 'api/Transaction/GetUnAssignedEmployess',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: Employees[]) => {
      this._unassignedEmp = [];
      this._unassignedEmp = results;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
      });
  }

  GetDashView() {
    this._busy = true;
    let token: string = localStorage.getItem("jwt");
    this._http.get(this._baseUrl + 'api/Transaction/GetDashView',
      {
        headers: new HttpHeaders({
          "Authorization": "Bearer " + token,
          "Content-Type": "application/json",
          "Cache-Control": "no-store",
          "Pragma": "no-cache"

        })
      }
    ).subscribe((results: ExcelListModel[]) => {
      this._dashList = [];
      this._dashList = results;

    },
      (error) => {
        console.log(error);
        this._busy = false;
        this._error = true;
        this._errMsg = error.message;
      },
      () => {
        this._busy = false;
        this.GetAssetsDetails();
        this.busy = true;
        setTimeout((a) => {
          this.GetUnAssignedAssets();

        }, 1000);

        this.busy = true;
        setTimeout((a) => {
          this.GetAssetsAssign();

        }, 1000);

        this.busy = true;
      })
  }

  ExportAssetList() {
      this._busy = true;
      let token: string = localStorage.getItem("jwt");
      this._http.get(this._baseUrl + 'api/Transaction/ExportAssetList',
        {
          headers: new HttpHeaders({
            "Authorization": "Bearer " + token,
            "Content-Type": "application/json",
            "Cache-Control": "no-store",
            "Pragma": "no-cache"

          })
        }
      ).subscribe((results: Files) => {
        let today: Date = new Date;
        let dateString: string;
        var a = document.createElement('a');
        a.target = "_blank";
        a.setAttribute('type', 'hidden');


        a.href = this._baseUrl + results.filename;
        a.download = results.filename;
        document.body.appendChild(a);
        a.click();
        a.remove();

      },
        (error) => {
          console.log(error);
          this._busy = false;
          this._error = true;
          this._errMsg = error.message;
        },
        () => {
          this._busy = false;
        });

    
  }

}
