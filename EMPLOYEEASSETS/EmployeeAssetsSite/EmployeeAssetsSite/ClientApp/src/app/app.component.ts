import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from './Service/app.service';
import { FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'app';


  constructor(public router: Router, public _appService: AppService, @Inject('BASE_URL') public baseUrl: string,
    private _http: HttpClient, private fb: FormBuilder, private fbc: FormBuilder) {
    if (this._appService.LoginService.IsLoggedIn) {
      router.navigateByUrl('/dashboard');

      
    } else {
      router.navigateByUrl('/login');


    }
  }
}
