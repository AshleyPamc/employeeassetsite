﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EmployeeAssetsSite.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace EmployeeAssetsSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Login : Base.Contex
    {

        public Login(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpPost]
        [Route("LoginDetails")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public LoginDetailsModel LoginDetails([FromBody] LoginDetailsModel model)
        {

            try
            {

                using (SqlConnection Sqlcon = new SqlConnection(_assetConnectionString))
                {

                    if (Sqlcon.State != ConnectionState.Open)
                    {
                        Sqlcon.Open();
                    }
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        //string sqlTextToExecute = "select count(*) from users where Username = '" + model.username + "' and Password = '" + model.password + "'";
                        string sqlTextToExecute = "select count(*) from users where Username = @username and Password = @password";

                        cmd.Parameters.Add("@username", SqlDbType.VarChar).Value = model.username;
                        cmd.Parameters.Add("@password", SqlDbType.VarChar, 20).Value = model.password;

                        cmd.Connection = Sqlcon;
                        cmd.CommandText = sqlTextToExecute;
                        Object o = cmd.ExecuteScalar();

                        if (o != null)
                        {
                            model.success = Convert.ToBoolean(o);
                        }


                        if (model.success == true)
                        {
                            sqlTextToExecute = "SELECT * from users WHERE (Users.Username = @username)";
                            cmd.Connection = Sqlcon;
                            cmd.CommandText = sqlTextToExecute;

                            DataTable dt = new DataTable();
                            dt.Load(cmd.ExecuteReader());

                            DataRow dr = dt.Rows[0];

                            model.username = dr["USERNAME"].ToString();
                            model.Firstname = dr["NAME"].ToString();
                            model.password = model.password;
                            model.createBy = dr["CREATEBY"].ToString();
                            model.createDate = Convert.ToDateTime(dr["CREATEDATE"]);
                            model.changeBy = dr["CHANGEBY"].ToString();
                            model.changeDate = Convert.ToDateTime(dr["CHANGEDATE"]);
                        }


                    }
                    Sqlcon.Close();
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                model.errorMessage = ex.InnerException.Message;
            }



            return model;

        }


        [HttpGet]
        [Route("validConnection")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel validConnection()
        {
            //Below variables will be used to check the connection of both databases in use
            ValidationResultsModel vr = new ValidationResultsModel();
            Boolean pamcortal;
            Boolean drcConnection;

            using (SqlConnection connection = new SqlConnection(_drcConnectionString))
            {
                try
                {
                    connection.Open();
                    drcConnection = true;
                    vr.valid = true;
                    connection.Close();
                }
                catch (SqlException ex)
                {
                    var msg = ex.Message;
                    drcConnection = false;
                    vr.valid = false;
                }

            }
            using (SqlConnection connection = new SqlConnection(_assetConnectionString))
            {
                try
                {
                    connection.Open();
                    pamcortal = true;
                    vr.valid = true;
                    connection.Close();
                }
                catch (SqlException)
                {
                    pamcortal = false;
                    vr.valid = false;
                }
            }
            if (drcConnection == true && pamcortal == true)
            {
                SqlConnectionStringBuilder drcIp = new SqlConnectionStringBuilder(_drcConnectionString);
                string[] drc = new string[drcIp.DataSource.Split(".").Length];
                drc = drcIp.DataSource.Split(".");
                SqlConnectionStringBuilder pamcIp = new SqlConnectionStringBuilder(_assetConnectionString);
                string[] pamc = new string[pamcIp.DataSource.Split(".").Length];
                pamc = pamcIp.DataSource.Split(".");

                vr.message = drc[drc.Length - 1] + pamc[pamc.Length - 1];
                vr.message = vr.message + " " + releaseDate;
                return vr;
            }
            else
            {
                vr.message = "";
                return vr;
            }
        }


        [HttpPost]
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "SECURITY";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\LOGIN\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {


            }


        }


    }
}
