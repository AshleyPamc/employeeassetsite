﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using EmployeeAssetsSite.Models;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace EmployeeAssetsSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Token : Base.Contex
    {
        private string fileName = "app.txt";

        public Token(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }

        [HttpPost("token")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ActionResult GetToken([FromBody] LoginDetailsModel data)
        {
            var token = new JwtSecurityToken();
            var root = _env.WebRootPath;
            var roles = new List<Claim>();

            DirectoryInfo di = new DirectoryInfo($"{root}\\tmp");
            FileInfo f = new FileInfo($"{root}\\tmp\\{ fileName }");
            string key;

            using (StreamReader reader = new StreamReader(f.FullName))
            {
                key = reader.ReadLine();
                reader.Close();
            }

            var symetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));

            var signingCredentials = new SigningCredentials(symetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);






                roles.Add(new Claim(ClaimTypes.Role, "NormalUser"));
                token = new JwtSecurityToken(
                    issuer: "pamcportal.in",
                    audience: "authorizedUsers",
                     expires: DateTime.Now.AddHours(8),
                    signingCredentials: signingCredentials,
                    claims: roles
               );
            



            return Ok(new JwtSecurityTokenHandler().WriteToken(token));

        }

    }
}
