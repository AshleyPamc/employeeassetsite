﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using EmployeeAssetsSite.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;

namespace EmployeeAssetsSite.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class Transaction : Base.Contex
    {
        public Transaction(IWebHostEnvironment env, IConfiguration con) : base(env, con)
        {

        }


        [HttpGet]
        [Route("GetDepartments")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<Departments> GetDepartments()
        {
            List<Departments> list = new List<Departments>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable ct = new DataTable();

                    cmd.CommandText = $"SELECT * FROM {AssetDatabase}.dbo.DEPARTMENTS";

                    ct.Load(cmd.ExecuteReader());

                    foreach (DataRow row in ct.Rows)
                    {
                        Departments d = new Departments();

                        d.id = row["ROWID"].ToString();
                        d.desc = row["DEPARTMENT"].ToString();
                        d.createBy = row["CREATEBY"].ToString();
                        d.createDate = row["CREATEDATE"].ToString();
                        d.changeBy = row["CHANGEBY"].ToString();
                        d.changeDate = row["CHANGEDATE"].ToString();
                        d.deleted = Convert.ToBoolean(row["DELETED"].ToString());
                        list.Add(d);
                    }


                }

            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }

        [HttpPost]
        [Route("UpdateDepartment")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel UpdateDepartment([FromBody] Departments data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));

                    cmd.CommandText = $"UPDATE {AssetDatabase}.dbo.DEPARTMENTS " +
                        $"SET DEPARTMENT = @desc, CHANGEBY = @by, CHANGEDATE = @date WHERE ROWID = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }

        [HttpPost]
        [Route("DeleteDepartment")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel DeleteDepartment([FromBody] Departments data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@deleted", data.deleted));

                    cmd.CommandText = $"UPDATE  {AssetDatabase}.dbo.DEPARTMENTS SET DELETED = @deleted, CHANGEBY = @by, CHANGEDATE = @date" +
                        $" WHERE ROWID = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }


        [HttpPost]
        [Route("NewDepartment")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel NewDepartment([FromBody] Departments data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));

                    cmd.CommandText = $"INSERT INTO {AssetDatabase}.dbo.DEPARTMENTS " +
                        $"(DEPARTMENT,CREATEBY,CREATEDATE,CHANGEBY,CHANGEDATE) VALUES " +
                        $"(@desc,@by,@date,@by,@date)";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }


        [HttpGet]
        [Route("GetWorkGroups")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<Departments> GetWorkGroups()
        {
            List<Departments> list = new List<Departments>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable ct = new DataTable();

                    cmd.CommandText = $"SELECT * FROM {AssetDatabase}.dbo.WORKGROUP";

                    ct.Load(cmd.ExecuteReader());

                    foreach (DataRow row in ct.Rows)
                    {
                        Departments d = new Departments();

                        d.id = row["ROWID"].ToString();
                        d.desc = row["WORKGROUP"].ToString();
                        d.createBy = row["CREATEBY"].ToString();
                        d.createDate = row["CREATEDATE"].ToString();
                        d.changeBy = row["CHANGEBY"].ToString();
                        d.changeDate = row["CHANGEDATE"].ToString();
                        d.deleted = Convert.ToBoolean(row["DELETED"].ToString());
                        list.Add(d);
                    }


                }

            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }

        [HttpPost]
        [Route("UpdateWorkGroup")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel UpdateWorkGroup([FromBody] Departments data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));

                    cmd.CommandText = $"UPDATE {AssetDatabase}.dbo.WORKGROUP " +
                        $"SET WORKGROUP = @desc, CHANGEBY = @by, CHANGEDATE = @date WHERE ROWID = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }

        [HttpPost]
        [Route("NewWorkGroup")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel NewWorkGroup([FromBody] Departments data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));

                    cmd.CommandText = $"INSERT INTO {AssetDatabase}.dbo.WORKGROUP " +
                        $"(WORKGROUP,CREATEBY,CREATEDATE,CHANGEBY,CHANGEDATE) VALUES " +
                        $"(@desc,@by,@date,@by,@date)";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }


        [HttpPost]
        [Route("DeleteWorkGroup")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel DeleteWorkGroup([FromBody] Groups data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@deleted", data.desc));

                    cmd.CommandText = $"UPDATE  {AssetDatabase}.dbo.WORKGROUP SET DELETED = @deleted, CHANGEBY = @by, CHANGEDATE = @date " +
                        $" WHERE ROWID = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }


        [HttpGet]
        [Route("GetEmployees")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<Employees> GetEmployees()
        {
            List<Employees> list = new List<Employees>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable ct = new DataTable();

                    cmd.CommandText = $"SELECT e.ROWID,e.DELETED,e.NAME,e.SURNAME,e.CELL,e.EMAIL,e.DEPARTMENT AS dep, e.WORKGROUP AS grp," +
                        $"ISNULL(e.OWNLAPTOP,'0') AS OWNLAPTOP,ISNULL(e.OWNWIFI,'0') AS OWNWIFI,ISNULL(e.UNCAPPED,'0') AS UNCAPPED,e.CREATEDATE,e.CHANGEDATE,e.CREATEBY,e.CHANGEBY,d.DEPARTMENT,w.WORKGROUP FROM {AssetDatabase}.dbo.EMPLOYEES AS e " +
                        $"INNER JOIN {AssetDatabase}.dbo.DEPARTMENTS d ON d.ROWID = e.DEPARTMENT " +
                        $"INNER JOIN {AssetDatabase}.dbo.WORKGROUP w ON w.ROWID = e.WORKGROUP ORDER BY e.NAME ASC";

                    ct.Load(cmd.ExecuteReader());

                    foreach (DataRow row in ct.Rows)
                    {
                        Employees d = new Employees();

                        d.id = row["ROWID"].ToString();
                        d.name = row["NAME"].ToString();
                        d.surname = row["SURNAME"].ToString();
                        d.cell = row["CELL"].ToString();
                        d.email = row["EMAIL"].ToString();
                        d.dep = row["dep"].ToString();
                        d.depDesc = row["DEPARTMENT"].ToString();
                        d.group = row["grp"].ToString();
                        d.groupDesc = row["WORKGROUP"].ToString();
                        d.laptop = Convert.ToBoolean(row["OWNLAPTOP"].ToString());
                        d.wifi = Convert.ToBoolean(row["OWNWIFI"].ToString());
                        d.uncapped = Convert.ToBoolean(row["UNCAPPED"].ToString());
                        d.createBy = row["CREATEBY"].ToString();
                        d.createDate = row["CREATEDATE"].ToString();
                        d.changeby = row["CHANGEBY"].ToString();
                        d.changeDate = row["CHANGEDATE"].ToString();
                        d.deleted = Convert.ToBoolean(row["DELETED"].ToString());
                        list.Add(d);
                    }


                }

            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }

        [HttpPost]
        [Route("UpdateEmployees")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel UpdateEmployees([FromBody] Employees data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeby));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@name", data.name));
                    cmd.Parameters.Add(new SqlParameter("@surname", data.surname));
                    cmd.Parameters.Add(new SqlParameter("@cell", data.cell));
                    cmd.Parameters.Add(new SqlParameter("@email", data.email));
                    cmd.Parameters.Add(new SqlParameter("@dep", data.dep));
                    cmd.Parameters.Add(new SqlParameter("@group", data.group));
                    cmd.Parameters.Add(new SqlParameter("@lap", data.laptop));
                    cmd.Parameters.Add(new SqlParameter("@wifi", data.wifi));
                    cmd.Parameters.Add(new SqlParameter("@uncap", data.uncapped));

                    cmd.CommandText = $"UPDATE {AssetDatabase}.dbo.EMPLOYEES " +
                        $"SET NAME = @name,SURNAME = @surname,CELL = @cell,EMAIL = @email,DEPARTMENT = @dep,WORKGROUP = @group,OWNLAPTOP=@lap,OWNWIFI = @wifi,UNCAPPED=@uncap" +
                        $", CHANGEBY = @by, CHANGEDATE = @date WHERE ROWID = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }

        [HttpPost]
        [Route("NewEmployees")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel NewEmployees([FromBody] Employees data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeby));
                    cmd.Parameters.Add(new SqlParameter("@name", data.name));
                    cmd.Parameters.Add(new SqlParameter("@surname", data.surname));
                    cmd.Parameters.Add(new SqlParameter("@cell", data.cell));
                    cmd.Parameters.Add(new SqlParameter("@email", data.email));
                    cmd.Parameters.Add(new SqlParameter("@dep", data.dep));
                    if (data.group == null || data.group == "")
                    {
                        cmd.Parameters.Add(new SqlParameter("@group", "6"));
                    }
                    else
                    {
                        cmd.Parameters.Add(new SqlParameter("@group", data.group));
                    }

                    cmd.Parameters.Add(new SqlParameter("@lap", data.laptop));
                    cmd.Parameters.Add(new SqlParameter("@wifi", data.wifi));
                    cmd.Parameters.Add(new SqlParameter("@uncap", data.uncapped));

                    cmd.CommandText = $"INSERT INTO {AssetDatabase}.dbo.EMPLOYEES " +
                        $"(NAME,SURNAME,CELL,EMAIL,DEPARTMENT,WORKGROUP,OWNLAPTOP,OWNWIFI,UNCAPPED,CREATEBY,CREATEDATE,CHANGEBY,CHANGEDATE) VALUES " +
                        $"(@name,@surname,@cell,@email,@dep,@group,@lap,@wifi,@uncap,@by,@date,@by,@date)";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }


        [HttpPost]
        [Route("DeleteEmployees")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel DeleteEmployees([FromBody] Employees data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by",data.changeby));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@deleted", data.deleted));

                    cmd.CommandText = $"UPDATE  {AssetDatabase}.dbo.EMPLOYEES SET DELETED = @deleted, CHANGEBY = @by, CHANGEDATE = @date " +
                        $" WHERE ROWID = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }

        [HttpGet]
        [Route("GetAssets")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<Assets> GetAssets()
        {
            List<Assets> list = new List<Assets>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable ct = new DataTable();

                    cmd.CommandText = $"SELECT e.ROWID,e.FRIENDLYNAME,e.DELETED,e.REGID,e.TYPE,e.CELL,e.CREATEDATE,e.CHANGEDATE,e.CHANGEBY,e.CREATEBY,d.DESCRIPTION " +
                        $"FROM {AssetDatabase}.dbo.ASSET AS e " +
                        $"INNER JOIN {AssetDatabase}.dbo.ASSET_DETAIL d ON d.TYPE = e.TYPE ORDER BY FRIENDLYNAME ASC ";


                    ct.Load(cmd.ExecuteReader());

                    foreach (DataRow row in ct.Rows)
                    {
                        Assets d = new Assets();

                        d.id = row["ROWID"].ToString();
                        d.regId = row["REGID"].ToString();
                        d.type = row["TYPE"].ToString();
                        d.cell = row["CELL"].ToString();
                        d.desc = row["DESCRIPTION"].ToString();
                        d.createBy = row["CREATEBY"].ToString();
                        d.createDate = row["CREATEDATE"].ToString();
                        d.changeBy = row["CHANGEBY"].ToString();
                        d.changeDate = row["CHANGEDATE"].ToString();
                        d.frName = row["FRIENDLYNAME"].ToString();
                        d.deleted = Convert.ToBoolean(row["DELETED"].ToString());
                        list.Add(d);
                    }


                }

            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }

        [HttpGet]
        [Route("ExportAssetList")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public Files ExportAssetList()
        {
            Files list = new Files();
            bool dirExist = false;
            string root = _env.WebRootPath;
            string fileName = $"ASSETSEXPORT_{DateTime.Now.ToString("yyyyMMddHHmmss")}.xlsx";
            FileInfo f = new FileInfo($"{root}\\{ fileName }");
            DirectoryInfo di = new DirectoryInfo($"{root}\\ClientApp\\src\\assets");

            try
            {

                if (di.Exists)
                {
                    dirExist = true;
                }
                else
                {
                    dirExist = false;
                }


                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable ct = new DataTable();

                    cmd.CommandText = $"SELECT e.ROWID,d.DESCRIPTION,e.FRIENDLYNAME,e.REGID,e.CELL,e.DELETED " +
                        $"FROM {AssetDatabase}.dbo.ASSET AS e " +
                        $"INNER JOIN {AssetDatabase}.dbo.ASSET_DETAIL d ON d.TYPE = e.TYPE ";


                    ct.Load(cmd.ExecuteReader());

                    //f.Create();

                    ExcelPackage.LicenseContext = LicenseContext.Commercial;
                    using (ExcelPackage package = new ExcelPackage(f))
                    {
                        ExcelWorksheet wss;
                        wss = package.Workbook.Worksheets.Add("assets");
                       
                        wss.Cells["A1"].LoadFromDataTable(ct, true);
                       // package.Save();
                        package.SaveAs(f);
                    }
                    list.filename = f.Name;
                    }

                Task.Delay(5000);

            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }


        [HttpPost]
        [Route("UpdateAssets")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel UpdateAssets([FromBody] Assets data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@cell", data.cell));
                    cmd.Parameters.Add(new SqlParameter("@regid", data.regId));
                    cmd.Parameters.Add(new SqlParameter("@type", data.type));
                    cmd.Parameters.Add(new SqlParameter("@frName", data.frName));

                    cmd.CommandText = $"UPDATE {AssetDatabase}.dbo.ASSET " +
                        $"SET CELL = @cell, TYPE = @type, REGID = @regid, FRIENDLYNAME = @frName" +
                        $", CHANGEBY = @by, CHANGEDATE = @date WHERE ROWID = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }

        [HttpPost]
        [Route("NewAssets")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel NewAssets([FromBody] Assets data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@cell", data.cell));
                    cmd.Parameters.Add(new SqlParameter("@regid", data.regId));
                    cmd.Parameters.Add(new SqlParameter("@type", data.type));
                    cmd.Parameters.Add(new SqlParameter("@frName", data.frName));

                    cmd.CommandText = $"INSERT INTO {AssetDatabase}.dbo.ASSET " +
                        $"(REGID,CELL,TYPE,FRIENDLYNAME,CREATEBY,CREATEDATE,CHANGEBY,CHANGEDATE) VALUES " +
                        $"(@regid,@cell,@type,@frName,@by,@date,@by,@date)";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }


        [HttpPost]
        [Route("DeleteAssets")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel DeleteAssets([FromBody] Assets data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@deleted", data.deleted));

                    cmd.CommandText = $"UPDATE  {AssetDatabase}.dbo.ASSET  SET DELETED = @deleted, CHANGEBY = @by, CHANGEDATE = @date" +
                        $" WHERE ROWID = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }


        [HttpGet]
        [Route("GetAssetsDetails")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<AssetDetails> GetAssetsDetails()
        {
            List<AssetDetails> list = new List<AssetDetails>();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    DataTable ct = new DataTable();

                    cmd.CommandText = $"SELECT * " +
                        $"FROM {AssetDatabase}.dbo.ASSET_DETAIL ";


                    ct.Load(cmd.ExecuteReader());

                    foreach (DataRow row in ct.Rows)
                    {
                        AssetDetails d = new AssetDetails();

                        d.type = row["TYPE"].ToString();
                        d.desc = row["DESCRIPTION"].ToString();
                        d.createBy = row["CREATEBY"].ToString();
                        d.createDate = row["CREATEDATE"].ToString();
                        d.changeBy = row["CHANGEBY"].ToString();
                        d.changeDate = row["CHANGEDATE"].ToString();
                        d.deleted = Convert.ToBoolean(row["DELETED"].ToString());

                        list.Add(d);
                    }


                }

            }
            catch (Exception ex)
            {

                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }

        [HttpPost]
        [Route("UpdateAssetsDetails")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel UpdateAssetsDetails([FromBody] AssetDetails data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@id", data.type));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));


                    cmd.CommandText = $"UPDATE {AssetDatabase}.dbo.ASSET_DETAIL " +
                        $"SET DESCRIPTION = @desc" +
                        $", CHANGEBY = @by, CHANGEDATE = @date WHERE TYPE = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }

        [HttpPost]
        [Route("NewAssetsDetails")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel NewAssetsDetails([FromBody] AssetDetails data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.desc));


                    cmd.CommandText = $"INSERT INTO {AssetDatabase}.dbo.ASSET_DETAIL " +
                        $"(DESCRIPTION,CREATEBY,CREATEDATE,CHANGEBY,CHANGEDATE) VALUES " +
                        $"(@desc,@by,@date,@by,@date)";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }


        [HttpPost]
        [Route("DeleteAssetsDetails")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel DeleteAssetsDetails([FromBody] AssetDetails data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();

            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != System.Data.ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();

                    cmd.Connection = cn;

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@id", data.type));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@deleted", data.deleted));

                    cmd.CommandText = $"UPDATE {AssetDatabase}.dbo.ASSET_DETAIL SET DELETED = @deleted, CHANGEBY = @by, CHANGEDATE = @date " +
                        $" WHERE TYPE = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;



                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }

            return vr;
        }


        [HttpGet]
        [Route("GetAssigments")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<AssetAssign> GetAssigments()
        {
            List<AssetAssign> list = new List<AssetAssign>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.CommandText = $"SELECT a.FRIENDLYNAME,a.REGID,ea.ROWID,ea.EMPLOYEEID,ea.ASSETID,ea.FROMDATE,ea.TODATE,ea.CREATEDATE,ea.CREATEBY," +
                        $"ea.CHANGEBY,ea.CHANGEDATE,(e.NAME + ' ' + e.SURNAME) AS EMPNAME,a.REGID,ad.DESCRIPTION FROM {AssetDatabase}.dbo.EMPLOYEE_ASSETS ea " +
                        $"INNER JOIN {AssetDatabase}.dbo.EMPLOYEES e ON e.ROWID = ea.EMPLOYEEID " +
                        $"INNER JOIN {AssetDatabase}.dbo.ASSET a ON a.ROWID = ea.ASSETID " +
                        $"INNER JOIN {AssetDatabase}.dbo.ASSET_DETAIL ad ON ad.TYPE = a.TYPE ";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        AssetAssign a = new AssetAssign();

                        a.id = row["ROWID"].ToString();
                        a.empId = row["EMPLOYEEID"].ToString();
                        a.empName = row["EMPNAME"].ToString();
                        a.assetId = row["ASSETID"].ToString();
                        a.assetRegId = row["REGID"].ToString();
                        a.assetName = row["DESCRIPTION"].ToString();
                        a.fromDate = row["FROMDATE"].ToString();
                        a.toDate = row["TODATE"].ToString();
                        a.createBy = row["CREATEBY"].ToString();
                        a.createDate = row["CREATEDATE"].ToString();
                        a.changeBy = row["CHANGEBY"].ToString();
                        a.changeDate = row["CHANGEDATE"].ToString();
                        a.frName = row["FRIENDLYNAME"].ToString();

                        list.Add(a);

                    }

                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }


        [HttpPost]
        [Route("DeleteAssetsAssing")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel DeleteAssetsAssing([FromBody] AssetAssign data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));

                    cmd.CommandText = $"DELETE FROM {AssetDatabase}.dbo.EMPLOYEE_ASSETS " +
                        $" WHERE ROWID = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;

                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("UpdateAssetsAssing")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel UpdateAssetsAssing([FromBody] AssetAssign data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@id", data.id));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));

                    cmd.Parameters.Add(new SqlParameter("@from", data.fromDate));
                    cmd.Parameters.Add(new SqlParameter("@to", data.toDate));


                    cmd.CommandText = $"UPDATE {AssetDatabase}.dbo.EMPLOYEE_ASSETS " +
                        $" SET  FROMDATE  = @from, TODATE = @to, CHANGEBY = @by, CHANGEDATE = @date " +
                        $"WHERE ROWID = @id";

                    cmd.ExecuteNonQuery();

                    vr.valid = true;

                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }
            return vr;
        }

        [HttpPost]
        [Route("NewAssetsAssing")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public ValidationResultsModel NewAssetsAssing([FromBody] AssetAssign data)
        {
            ValidationResultsModel vr = new ValidationResultsModel();
            try
            {
                string id = "";
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")));
                    cmd.Parameters.Add(new SqlParameter("@by", data.changeBy));
                    cmd.Parameters.Add(new SqlParameter("@empid", data.empId));
                    cmd.Parameters.Add(new SqlParameter("@assetid", data.assetId));
                    cmd.Parameters.Add(new SqlParameter("@from", data.fromDate));
                    cmd.Parameters.Add(new SqlParameter("@to", data.toDate));


                    if(data.toDate == "" || data.toDate == null || data.toDate.Length < 10)
                    {
                        cmd.CommandText = $"INSERT INTO {AssetDatabase}.dbo.EMPLOYEE_ASSETS " +
    $" (EMPLOYEEID, ASSETID,FROMDATE,TODATE ,CHANGEBY, CHANGEDATE,CREATEBY,CREATEDATE) OUTPUT INSERTED.ROWID " +
    $"VALUES" +
    $" ( @empid,@assetid,@from,NULL,@by,@date,@by,@date) ";
                    }
                    else
                    {
                        cmd.CommandText = $"INSERT INTO {AssetDatabase}.dbo.EMPLOYEE_ASSETS " +
    $" (EMPLOYEEID, ASSETID,FROMDATE,TODATE ,CHANGEBY, CHANGEDATE,CREATEBY,CREATEDATE) OUTPUT INSERTED.ROWID " +
    $"VALUES" +
    $" ( @empid,@assetid,@from,@to,@by,@date,@by,@date) ";
                    }



                   id =  cmd.ExecuteScalar().ToString();

                    vr.valid = true;

                    try
                    {
                        using (SqlConnection cnn = new SqlConnection(_drcConnectionString))
                        {
                            if (cnn.State != ConnectionState.Open)
                            {
                                cnn.Open();
                            }

                            DataTable dtt = new DataTable();
                            SqlCommand cmdd = new SqlCommand();
                            cmdd.Connection = cnn;

                            cmdd.CommandText = $"SELECT * FROM {DRCDatabase}..EDI_MAILSERVERS WHERE ROWID = '3'";
                            dtt.Load(cmdd.ExecuteReader());

                            string server = "";
                            string username = "";
                            string password = "";
                            string from = "";
                            string port = "";

                            foreach (DataRow row in dtt.Rows)
                            {
                                server = row["SERVERADDRESS"].ToString();
                                password = row["PASSWORD"].ToString();
                                username = row["USERNAME"].ToString();
                                from = row["MAILFROM"].ToString();
                                port = row["OUTGOINGPORT"].ToString();
                            }

                            string toAddress = "";

                            dtt = new DataTable();
                            cmdd.Parameters.Add(new SqlParameter("@empid", data.empId));
                            cmdd.CommandText = $"SELECT EMAIL FROM {AssetDatabase}.dbo.EMPLOYEES WHERE ROWID = @empid";

                            dtt.Load(cmdd.ExecuteReader());

                            toAddress = dtt.Rows[0][0].ToString();

                            dtt = new DataTable();

                            cmdd.Parameters.Add(new SqlParameter("@assId", id));


                            cmdd.CommandText = $"SELECT a.ROWID, \n"
           + "       a.REGID, \n"
           + "       a.CELL, \n"
           + "       ad.DESCRIPTION, \n"
           + "       ea.ASSETID\n"
           + $"FROM {AssetDatabase}.dbo.ASSET a\n"
           + $"     INNER JOIN {AssetDatabase}.dbo.EMPLOYEE_ASSETS  ea ON  ea.ASSETID = a.ROWID \n"
           + $"     INNER JOIN {AssetDatabase}.dbo.ASSET_DETAIL ad ON ad.TYPE = a.TYPE  " +
           $"WHERE ea.ROWID = @assId";

                            dtt.Load(cmdd.ExecuteReader());

                            string regid = "";
                            string desc = "";

                            foreach (DataRow row in dtt.Rows)
                            {
                                regid = row["REGID"].ToString();
                                desc = row["DESCRIPTION"].ToString();
                            }

                            string subject = $"New Asset Assignment {DateTime.Now.ToString("yyy/MM/dd")}";

                            string body = $"Good day, \r\n\r\n" +
                                $"A new {desc} was assigned to you with registration id : {regid}. \r\n" +
                                $"If this information is not correct please send a email to {data.changeBy}. \r\n\r\n" +
                                $"Regards\r\n" +
                                $"PAMC TEAM";


                            bool sended = PAMC.EmailSender.Sender.SendEmailViaSMTP(server, username, password, from, subject, body, toAddress, null);
                        }
                        }
                    catch (Exception ex)
                    {

                        
                    }

                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                vr.valid = false;
                throw;
            }
            return vr;
        }

        [HttpGet]
        [Route("GetUnAssignedAssets")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<Assets> GetUnAssignedAssets()
        {
            List<Assets> list = new List<Assets>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.CommandText = $"SELECT DISTINCT a.ROWID, a.FRIENDLYNAME,a.TYPE,ad.DESCRIPTION,a.REGID,a.CELL " +
                     $"FROM {AssetDatabase}.dbo.ASSET a " +
                     $"INNER JOIN {AssetDatabase}.dbo.ASSET_DETAIL ad ON ad.TYPE = a.TYPE " +
                     $"LEFT OUTER JOIN {AssetDatabase}.dbo.EMPLOYEE_ASSETS ea ON ea.ASSETID = a.ROWID " +
                     $"WHERE GETDATE() >= ISNULL(ea.TODATE, '2030/12/31') " +
                     $" AND a.ROWID NOT IN(SELECT ASSETID FROM EMPLOYEE_ASSETS WHERE TODATE IS NULL) " +
                     $"OR a.ROWID NOT IN( " +
                     $"SELECT ASSETID FROM {AssetDatabase}.dbo.EMPLOYEE_ASSETS) ";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Assets a = new Assets();
                        a.id = row["ROWID"].ToString();
                        a.regId = row["REGID"].ToString();
                        a.desc = row["DESCRIPTION"].ToString();
                        a.cell= row["CELL"].ToString();
                        a.type = row["TYPE"].ToString();
                        a.frName = row["FRIENDLYNAME"].ToString();
                        list.Add(a);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }

        [HttpGet]
        [Route("GetUnAssignedEmployess")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<Assets> GetUnAssignedEmployess()
        {
            List<Assets> list = new List<Assets>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.CommandText = $"SELECT a.ROWID " +
                     $"FROM {AssetDatabase}.dbo.EMPLOYEES a " +
                     $"LEFT OUTER JOIN {AssetDatabase}.dbo.EMPLOYEE_ASSETS ea ON ea.EMPLOYEEID = a.ROWID " +
                     $"WHERE GETDATE() >= ISNULL(ea.TODATE, '2030/12/31') " +
                     $"OR a.ROWID NOT IN( " +
                     $"SELECT EMPLOYEEID FROM {AssetDatabase}.dbo.EMPLOYEE_ASSETS) ";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        Assets a = new Assets();
                        a.id = row["ROWID"].ToString();
                        list.Add(a);
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }

        [HttpGet]
        [Route("GetDashView")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<ExcelListModel> GetDashView()
        {
            List<ExcelListModel> list = new List<ExcelListModel>();
            try
            {
                using(SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if(cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    string sql = $"SELECT * FROM {AssetDatabase}.dbo.AssetCombinedView a ORDER BY a.NAME\n";
                               

                    cmd.CommandText = sql;
                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        ExcelListModel e = new ExcelListModel();

                        e.emId = row["ROWID"].ToString();
                        e.name = row["NAME"].ToString();
                        e.surname = row["SURNAME"].ToString();
                        e.department = row["DEPARTMENT"].ToString();
                      //  e.assign = Convert.ToBoolean(row["ASSIGNED"].ToString());
                        e.laptop = Convert.ToBoolean(row["OWNLAPTOP"].ToString());
                        e.wifi = Convert.ToBoolean(row["OWNWIFI"].ToString());
                        e.group = row["WORKGROUP"].ToString();

                       // e.pamcScreen = Convert.ToBoolean(row["PAMCSCREEN"].ToString());
                        if (row["PAMCSCREEN"].ToString() == "0")
                        {
                            e.pamcScreen = false;
                        }
                        else if (row["PAMCSCREEN"].ToString() == "1")
                        {
                            e.pamcScreen = true;
                        }
                       // e.pamcLaptop = Convert.ToBoolean(row["PAMCLAPTOP"].ToString());
                        if (row["PAMCLAPTOP"].ToString() == "0")
                        {
                            e.pamcLaptop = false;
                        }
                        else if (row["PAMCLAPTOP"].ToString() == "1")
                        {
                            e.pamcLaptop = true;
                        }
                       // e.pamcDongle = Convert.ToBoolean(row["PAMCDONGLE"].ToString());
                        if (row["PAMCDONGLE"].ToString() == "0")
                        {
                            e.pamcDongle = false;
                        }
                        else if (row["PAMCDONGLE"].ToString() == "1")
                        {
                            e.pamcDongle = true;
                        }
                        list.Add(e);

                    }

                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }


        [HttpPost]
        public void LogError(string error, string stacktrace)
        {
            string date = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            string fileName = "";
            string controler = "SECURITY";
            var root = _env.WebRootPath;

            fileName = $"E_{controler}_{date}.txt";

            FileInfo fi = new FileInfo($"{root}\\Error_Log\\LOGIN\\{fileName}");
            //  fi.Create();

            try
            {
                using (StreamWriter fs = new StreamWriter(fi.FullName))
                {
                    fs.Write("ERROR: \n\n" + error + "\n\n STACK TRACE: \n\n" + stacktrace);
                }
            }
            catch (Exception)
            {


            }


        }


        [HttpPost]
        [Route("GetAssigmentsPerEmployee")]
        [Authorize(Roles = "NormalUser")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public List<AssetAssign> Employee([FromBody] ExcelListModel data)
        {
            List<AssetAssign> list = new List<AssetAssign>();
            try
            {
                using (SqlConnection cn = new SqlConnection(_assetConnectionString))
                {
                    if (cn.State != ConnectionState.Open)
                    {
                        cn.Open();
                    }

                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cn;

                    DataTable dt = new DataTable();

                    cmd.Parameters.Add(new SqlParameter("@id", data.emId));
                    cmd.CommandText = $"SELECT a.FRIENDLYNAME,a.REGID,ea.ROWID,ea.EMPLOYEEID,ea.ASSETID,ea.FROMDATE,ea.TODATE,ea.CREATEDATE,ea.CREATEBY," +
                        $"ea.CHANGEBY,ea.CHANGEDATE,(e.NAME + ' ' + e.SURNAME) AS EMPNAME,a.REGID,ad.DESCRIPTION FROM {AssetDatabase}.dbo.EMPLOYEE_ASSETS ea " +
                        $"INNER JOIN {AssetDatabase}.dbo.EMPLOYEES e ON e.ROWID = ea.EMPLOYEEID " +
                        $"INNER JOIN {AssetDatabase}.dbo.ASSET a ON a.ROWID = ea.ASSETID " +
                        $"INNER JOIN {AssetDatabase}.dbo.ASSET_DETAIL ad ON ad.TYPE = a.TYPE " +
                        $"WHERE ea.EMPLOYEEID = @id";

                    dt.Load(cmd.ExecuteReader());

                    foreach (DataRow row in dt.Rows)
                    {
                        AssetAssign a = new AssetAssign();

                        a.id = row["ROWID"].ToString();
                        a.empId = row["EMPLOYEEID"].ToString();
                        a.empName = row["EMPNAME"].ToString();
                        a.assetId = row["ASSETID"].ToString();
                        a.assetRegId = row["REGID"].ToString();
                        a.assetName = row["DESCRIPTION"].ToString();
                        a.fromDate = row["FROMDATE"].ToString();
                        a.toDate = row["TODATE"].ToString();
                        a.createBy = row["CREATEBY"].ToString();
                        a.createDate = row["CREATEDATE"].ToString();
                        a.changeBy = row["CHANGEBY"].ToString();
                        a.changeDate = row["CHANGEDATE"].ToString();
                        a.frName = row["FRIENDLYNAME"].ToString();

                        list.Add(a);

                    }

                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, ex.StackTrace);
                throw;
            }
            return list;
        }



    }
}
