﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAssetsSite.Models
{
    public class AssetAssign
    {
        public string id { get; set; }
        public string empId { get; set;}
        public string empName { get; set; }
        public string assetId { get; set; }
        public string assetRegId { get; set; }
        public string assetName { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string createDate { get; set; }
        public string changeDate { get; set; }
        public string createBy { get; set; }
        public string changeBy { get; set; }
        public string frName { get; set; }
    }
}
