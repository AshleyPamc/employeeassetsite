﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAssetsSite.Models
{
    public class Assets
    {
        public string id { get; set; }
        public string regId { get; set; }
        public string type { get; set; }
        public string desc { get; set; }
        public string cell { get; set; }
        public string createBy { get; set; }
        public string createDate { get; set; }
        public string changeDate { get; set; }
        public string changeBy { get; set; }
        public bool deleted { get; set; }
        public string frName { get; set; }
        public bool selected { get; set; }
        public bool assigned { get; set; }
    }
}
