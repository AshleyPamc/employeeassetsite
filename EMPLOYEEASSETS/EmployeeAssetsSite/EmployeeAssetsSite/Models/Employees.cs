﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAssetsSite.Models
{
    public class Employees
    {
        public string id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public string cell { get; set; }
        public string email { get; set; }
        public string dep { get; set; }
        public string depDesc { get; set; }
        public string group { get; set; }
        public string groupDesc { get; set; }
        public bool laptop { get; set; }
        public bool wifi { get; set; }
        public bool uncapped { get; set; }
        public string createDate { get; set; }
        public string createBy { get; set; }
        public string changeDate { get; set; }
        public string changeby { get; set; }
        public bool deleted { get; set; }
        public bool selected { get; set; }
        public bool assigned { get; set; }
    }
}
