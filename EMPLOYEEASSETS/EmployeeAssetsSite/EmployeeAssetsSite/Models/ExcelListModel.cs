﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAssetsSite.Models
{
    public class ExcelListModel
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string department { get; set; }
        public bool laptop { get; set; }
        public bool wifi { get; set; }
        public bool uncaped { get; set; }
        public bool assign { get; set; }
        public string group { get; set; }
        public string assetType { get; set; }
        public string regId { get; set; }
        public string frName { get; set; }
        public string cell { get; set; }
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public bool pamcLaptop { get; set; }
        public bool pamcDongle { get; set; }
        public bool pamcScreen { get; set; }
        public string emId { get; set; }
    }
}
