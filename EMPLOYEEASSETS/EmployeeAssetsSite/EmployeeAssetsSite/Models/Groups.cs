﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAssetsSite.Models
{
    public class Groups
    {
        public string id { get; set; }
        public string desc { get; set; }
        public string createBy { get; set; }
        public string createDate { get; set; }
        public string changeBy { get; set; }
        public string changeDate { get; set; }
        public bool deleted { get; set; }
    }
}
