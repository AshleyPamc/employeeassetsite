﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmployeeAssetsSite.Models
{
    public class LoginDetailsModel
    {
        public string username { get; set; }
        public string password { get; set; }
        public string newPassword { get; set; }
        public int userType { get; set; }
        public string createBy { get; set; }
        public DateTime createDate { get; set; }
        public string changeBy { get; set; }
        public DateTime changeDate { get; set; }
        public bool success { get; set; }
        public bool dentist { get; set; }
        public bool specialist { get; set; }

        public string[] listOfproviders { get; set; }
        public DateTime transactionDate { get; set; }
        public string errorMessage { get; set; }
        public string provID { get; set; }
        public string bureauId { get; set; }
        public string brokerId { get; set; }
        public string SpecCode { get; set; }
        public string description { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string contact { get; set; }
        public string contract { get; set; }
        public int chprefix { get; set; }
        public string lobcode { get; set; }
        public List<string> lobdesc { get; set; }
        public string client { get; set; }
        public string memberIdNumber { get; set; }
        public string memberNumber { get; set; }
    }
}
